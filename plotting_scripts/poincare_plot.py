import os, sys
import numpy as np
import h5py as hf

import matplotlib as mpl
import matplotlib.pyplot as plt

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import vmecTools.wout_files.wout_read as wr


tor_ang = 0.25*np.pi

poin_name = 'poincare_989-1-1_roa0p8_D1p29_K1p05.h5'
# poin_name = 'poincare_56-1-728_roa0p8_Dnan_K0p96.h5'

data_dirc = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files')
conID = poin_name.split('_')[1]

# Get Iota Profile #
mainID = 'main_coil_'+conID.split('-')[0]
setID = 'set_'+conID.split('-')[1]
jobID = 'job_'+conID.split('-')[2]
wout_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID)
wout = wr.readWout(wout_file)

s_grid = wout.s_grid
iota = wout.iota(s_grid)

# Get Poincare Data #
poin_file = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', poin_name)
with hf.File(poin_file, 'r') as hf_:
    t_dom = hf_['core'][0, :, 2]
    dt = t_dom[1] - t_dom[0]

    npts = t_dom.shape[0]
    stps = int(round(2 * np.pi / dt))
    rots = int(round(t_dom[int(npts-1)] / (2*np.pi)))

    idx_stps = np.argmin(np.abs(t_dom - tor_ang)) + [int(i*stps) for i in range(rots)]

    poin_pnts = hf_['core'][:, idx_stps, :]

# Get Elongation Ratio #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

conArr = np.array([int(iD) for iD in conID.split('-')])
idx = np.argmin(np.sum(np.abs(met_data[:, 0:3] - conArr), axis=1))
K_val = met_data[idx, 9]

# Get Vessel Wall #
path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'vessel90.h5')
with hf.File(path,'r') as hf_file:
    vessel = hf_file['data'][:]
    v_dom = hf_file['domain'][:]

idx = np.argmin(np.abs(v_dom - tor_ang))
ves_x = vessel[idx, :, 0]
ves_y = vessel[idx, :, 1]
ves_z = vessel[idx, :, 2]
ves_r = np.hypot(ves_x, ves_y)

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Plotting Axis #
fig = plt.figure(tight_layout=True, figsize=(14, 6))
gs = mpl.gridspec.GridSpec(1, 3)

ax1 = fig.add_subplot(gs[0:2])
ax2 = fig.add_subplot(gs[2])

ax1.set_aspect('equal')

# Plot Poincare Data #
ax1.scatter(poin_pnts[:, :, 0], poin_pnts[:, :, 1], c='k', s=1)
ax1.plot(ves_r, ves_z, c='k', lw=4)

# Plot Iota Profile #
ax2.plot(np.sqrt(s_grid), iota, c='k')
# ax2.plot([0, 1], [1.]*2, c='k', ls='--')
ax2.plot([0, 1], [8./7]*2, c='k', ls='--')
ax2.plot([0, 1], [12./10]*2, c='k', ls='-.')
ax2.plot([0, 1], [16./13]*2, c='k', ls=':')
# ax2.plot([0, 1], [4./3]*2, c='k', ls=':')

# Axis Text #
# ax2.text(0.75, 0.995, '4/4', fontsize=16)
ax2.text(0.75, (8./7)+.002, '8/7', fontsize=16)
ax2.text(0.25, 1.2+.002, '12/10', fontsize=16)
ax2.text(0.25, (16./13)+.002, '16/13', fontsize=16)
# ax2.text(0.25, (4./3)-.013, '4/3', fontsize=16)

# Axis Limits #
ax1.set_xlim(0.83, 1.28)
ax1.set_ylim(-.17, .17)
ax2.set_xlim(0, 1)

# Axis Labels #
ax1.set_xlabel(r'$R \ (m)$')
ax1.set_ylabel(r'$Z \ (m)$')

ax2.set_xlabel(r'$r/a$')
ax2.set_ylabel(r'$\iota/2\pi$')

# Axis Title #
# ax1.set_title(r'$D = {0:0.2g} \%, \ \mathcal{{K}}/\mathcal{{K}}^* = {1:0.2f}$'.format(D_val, K_val))
ax1.set_title(r'$\mathcal{{K}}/\mathcal{{K}}^* = {0:0.2f}$'.format(K_val))

# Axis Grids #
ax2.grid()

# Show/Save
# plt.show()

save_name = poin_name.split('.')[0]+'.png'
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', save_name)
plt.savefig(save_path)
