import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt


# Read in Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22

# Plotting Axis #
fig, axs = plt.subplots(2, 1, sharex=True, sharey=False, tight_layout=True, figsize=(8, 10))

# Axis Plot #
axs[0].scatter(met_data[:, 9], met_data[:, 7], c='None', edgecolor='tab:blue', s=150)
axs[0].scatter([1.], [1.], c='k', edgecolor='w', marker='*', s=500)

axs[1].scatter(met_data[:, 9], met_data[:, 6], c='None', edgecolor='tab:orange', s=150)
axs[1].scatter([1.], [1.], c='k', edgecolor='w', marker='*', s=500)

# Axis Limits #
axs[1].set_xlim(0.955, 1.08)

# Axis Labels #
axs[0].set_ylabel(r'$\mathcal{G}_{\psi} / \mathcal{G}_{\psi}^*$')
axs[1].set_ylabel(r'$\mathcal{F}_{\kappa} / \mathcal{F}_{\kappa}^*$')
axs[1].set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

# Axis Grids #
axs[0].grid()
axs[1].grid()

# Axis Show/Save #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'selection_kappa.png')
plt.savefig(save_path)
# plt.show()
