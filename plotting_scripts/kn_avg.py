import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import gistTools.gist_reader as gr


# Import Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Calculate Kn Average #
kn_avg = np.empty(met_data.shape[0])
for mdx, met in enumerate(met_data):
    conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
    print(conID+' ({0:0.0f}|{1:0.0f})'.format(mdx+1, met_data.shape[0]))

    gist_path = os.path.join('/mnt', 'GENE', 'linear_data', conID, 'ky_0p1', 'gist_1_1')
    gist = gr.read_gist(gist_path)

    kn = gist.data['Kn']/gist.minor
    jacob = gist.data['Jacob'] * (2*gist.q0/(gist.minor**3))
    kn_avg[mdx] = np.trapz(kn*jacob, gist.pol_dom) / np.trapz(jacob, gist.pol_dom)

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 22}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 26

# Plotting Axis #
fig, ax = plt.subplots(1, 1, tight_layout=True)

# Plot <k_n> #
ax.scatter(met_data[:, 9], kn_avg, marker='o', facecolor='None', edgecolor='tab:red', s=150)

# Axis Labels #
ax.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax.set_ylabel(r'$\langle \kappa_n \rangle$')

ax.grid()

plt.show()
