import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.gridspec as gsp
import matplotlib.pyplot as plt

from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import ImageGrid

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

from vmecTools.wout_files import wout_read as wr
from vmecTools.wout_files import curveB_tools as curB


tor_angle = 0.0*np.pi

# Import Metric Data #
metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Identify Configurations to Plot #
idx_min = np.argmin(met_data[:, 9])
idx_max = np.argmax(met_data[:, 9])

kappa_min = met_data[idx_min, 9]
kappa_max = met_data[idx_max, 9]

conID_min = '-'.join(['{0:0.0f}'.format(iD) for iD in met_data[idx_min, 0:3]])
conID_max = '-'.join(['{0:0.0f}'.format(iD) for iD in met_data[idx_max, 0:3]])

conIDs = [conID_min, '0-1-0', conID_max]
kappa_values = np.array([kappa_min, 1., kappa_max])

# Import Vessel Data #
path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'vessel90.h5')
with hf.File(path,'r') as hf_file:
    vessel = hf_file['data'][:]
    v_dom = hf_file['domain'][:]

idx = np.argmin(np.abs(v_dom - tor_angle))
ves_x = vessel[idx, :, 0]
ves_y = vessel[idx, :, 1]
ves_z = vessel[idx, :, 2]
ves_r = np.hypot(ves_x, ves_y)

# Get Surface Data #
psi_dom = np.linspace(.2, 1, 5)**2
pol_dom = np.linspace(-np.pi, np.pi, 501)
flux_surf = np.empty((len(conIDs), psi_dom.shape[0], pol_dom.shape[0], 2))

k_n = {}
R_grid = {}
Z_grid = {}

for cdx, conID in enumerate(conIDs):
    conArr = np.array([float(iD) for iD in conID.split('-')])
    mainID = 'main_coil_{0:0.0f}'.format(conArr[0])
    setID = 'set_{0:0.0f}'.format(conArr[1])
    jobID = 'job_{0:0.0f}'.format(conArr[2])
    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID)

    wout = wr.readWout(wout_path, space_derivs=True, field_derivs=True)
    ampKeys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv',
               'Bmod', 'Bs_covar', 'Bu_covar', 'Bv_covar',
               'dBmod_ds', 'dBmod_du', 'dBmod_dv',
               'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']

    beg_idx = np.argmin(np.abs(wout.s_grid - 0.2**2))
    wout.transForm_2D_vSec(wout.s_grid[beg_idx::], pol_dom, tor_angle, ampKeys)

    curve = curB.BcurveTools(wout)
    curve.calc_curvature()

    for psi_idx, psi_val in enumerate(psi_dom):
        idx = np.argmin(np.abs(wout.s_grid[beg_idx::] - psi_val))
        flux_surf[cdx, psi_idx] = np.stack((wout.invFourAmps['R'][idx], wout.invFourAmps['Z'][idx]), axis=1)

    k_n[conID] = curve.k_s
    R_grid[conID] = wout.invFourAmps['R']
    Z_grid[conID] = wout.invFourAmps['Z']

k_n_min = np.inf
k_n_max = -np.inf
for key in conIDs:
    if np.min(k_n[key]) < k_n_min:
        k_n_min = np.min(k_n[key])
    if np.max(k_n[key]) > k_n_max:
        k_n_max = np.max(k_n[key])
k_n_lim = np.max(np.abs(np.array([k_n_min, k_n_max])))

# Plot Axis #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Plotting Axis #
fig = plt.figure(figsize=(9, 6))
axs = ImageGrid(fig, 111,
                nrows_ncols=(1, 3),
                axes_pad=0.25,
                aspect=True,
                cbar_location='right',
                cbar_mode='single',
                cbar_size='10%',
                cbar_pad=0.15)

# Plot Surfaces #
axs[0].pcolormesh(R_grid[conIDs[0]], Z_grid[conIDs[0]], k_n[conIDs[0]], vmin=-k_n_lim, vmax=k_n_lim, cmap='seismic')
axs[1].pcolormesh(R_grid[conIDs[1]], Z_grid[conIDs[1]], k_n[conIDs[1]], vmin=-k_n_lim, vmax=k_n_lim, cmap='seismic')
smap = axs[2].pcolormesh(R_grid[conIDs[2]], Z_grid[conIDs[2]], k_n[conIDs[2]], vmin=-k_n_lim, vmax=k_n_lim, cmap='seismic')

for cdx, conID in enumerate(conIDs):
    axs[cdx].plot(ves_r, ves_z, c='k', lineWidth=3)
    for pdx in range(psi_dom.shape[0]):
        axs[cdx].plot(flux_surf[cdx, pdx, :, 0], flux_surf[cdx, pdx, :, 1], c='w')

    # Axis Titles #
    axs[cdx].set_title(r'$\mathcal{{K}} / \mathcal{{K}}^* = {0:0.2f}$'.format(kappa_values[cdx]))

# Axis Colorbar #
plt.colorbar(smap, cax=axs.cbar_axes[0])
axs.cbar_axes[0].set_ylabel(r'$\kappa_n$')

# Axis Labels #
axs[0].set_ylabel('Z')
axs[0].set_xlabel('R')
axs[1].set_xlabel('R')
axs[2].set_xlabel('R')

# Axis Limits #
axs[0].set_ylim(-.3, .3)
axs[1].set_ylim(-.3, .3)
axs[2].set_ylim(-.3, .3)

# Axis Text #
x_txt, y_txt = 1.505, -0.28
axs[0].text(x_txt, y_txt, '(a)')
axs[1].text(x_txt, y_txt, '(b)')
axs[2].text(x_txt, y_txt, '(c)')

# Save/Show Figure #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'elongation_sample.png')
# plt.savefig(save_path)
plt.show()
