import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import gistTools.gist_reader as gr


runID = "1"
conIDs = ['5-1-485', '0-1-0', '897-1-0']
ky_tags = ['ky_0p1', 'ky_0p4', 'ky_0p7']

# Gen runIDs #
runIDs = np.empty((len(conIDs), len(ky_tags)), dtype=object)
base_path = os.path.join('/mnt', 'GENE', 'linear_data')
for i, conID in enumerate(conIDs):
    for j, ky_tag in enumerate(ky_tags):
        eigen_path = os.path.join(base_path, conID, ky_tag)
        iDs = np.array([int(f.name.split('_')[2]) for f in os.scandir(eigen_path)
                      if f.name.split('_')[0] == 'parameters' and f.name.split('_')[1] == runID])
        run = np.max(iDs)
        runIDs[i, j] = runID+'_{0:0.0f}'.format(run)

# Import Eigendata #
kappa = {}
time = {}
bloon = {}
eigen = {}

pol_dom = {}
modB = {}
kn = {}

eigen_set = {}
theta_set = {}

for i, conID in enumerate(conIDs):
    for j, ky_tag in enumerate(ky_tags):
        # Import Metric Data #
        metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
        with hf.File(metric_path, 'r') as hf_:
            met_data = hf_['metric data'][()]
            if conID == '0-1-0':
                kappa[conID+'/'+ky_tag] = 1.
            else:
                conArr = np.array([float(iD) for iD in conID.split('-')])
                idx = np.argmin(np.sum(np.abs(met_data[:, 0:3] - conArr), axis=1))
                kappa[conID+'/'+ky_tag] = met_data[idx, 9]

        # Read in Eigendata #
        eigen_path = os.path.join(base_path, conID, ky_tag, 'eigendata_'+runIDs[i, j]+'.h5')
        with hf.File(eigen_path, 'r') as hf_:
            time[conID+'/'+ky_tag] = hf_['time'][()]
            bloon[conID+'/'+ky_tag] = hf_['ballooning angle'][()]
            eigen[conID+'/'+ky_tag] = hf_['eigenfunction'][0]

        # Import GIST Data #
        gist_path = os.path.join('/mnt', 'GENE', 'linear_data', conID, ky_tag, 'gist_'+runIDs[i, j])
        gist = gr.read_gist(gist_path)

        pol_dom[conID+'/'+ky_tag] = gist.pol_dom/np.pi
        modB[conID+'/'+ky_tag] = gist.data['modB']
        kn[conID+'/'+ky_tag] = gist.data['Kn']

        # Import Simulation Parameters #
        pram_path = os.path.join('/mnt', 'GENE', 'linear_data', conID, ky_tag, 'parameters_'+runIDs[i, j])
        with open(pram_path, 'r') as f:
            lines = f.readlines()
            for line in lines:
                line = line.strip().split()
                if len(line) > 0 and line[0] == 'n_pol':
                    npol = int(line[2])
                if len(line) > 0 and line[0] == 'nx0':
                    nx0 = int(line[2])
                if len(line) > 0 and line[0] == 'nz0':
                    nz0 = int(line[2])
                if len(line) > 0 and line[0] == 'lx':
                    lx = float(line[2])

        # Ballooning Partitions and Radial Wavenumbers #
        if nx0 % 2 == 1:
            n_dom = np.linspace(-0.5*(nx0-1), 0.5*(nx0-1), nx0)
        else:
            n_dom = np.linspace(-.5*(nx0-2), .5*(nx0-2), nx0)

        balloon_angles = np.stack((2*n_dom-1, 2*n_dom+1), axis=1) * (npol*np.pi)
        kx_dom = n_dom * (2*np.pi/lx)

        eigen_dom = np.zeros(nz0)
        theta_dom = np.linspace(-npol, npol, nz0)

        for idx, balloon_range in enumerate(balloon_angles):
            bln = np.array(bloon[conID+'/'+ky_tag])
            eig = np.array(eigen[conID+'/'+ky_tag])

            z_dom = bln[(bln >= balloon_range[0]) & (bln < balloon_range[1])]
            eigen_sub = eig[(bln >= balloon_range[0]) & (bln < balloon_range[1])]
            eigen_dom = eigen_dom + (eigen_sub * np.conj(eigen_sub)).real

        eigen_set[conID+'/'+ky_tag] = eigen_dom / np.max(eigen_dom)
        theta_set[conID+'/'+ky_tag] = theta_dom

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22

fig, axs = plt.subplots(len(ky_tags), len(conIDs), sharex=True, sharey=True, tight_layout=True, figsize=(8+6*(len(conIDs)-1), 6+2*(len(ky_tags)-1)))

for i, ky_tag in enumerate(ky_tags):
    for j, conID in enumerate(conIDs):
        key = conID+'/'+ky_tag
        ax_twin = axs[i, j].twinx()
        axn, = axs[i, j].plot(theta_set[key], eigen_set[key], c='tab:blue')
        twn, = ax_twin.plot(pol_dom[key], modB[key], c='tab:red')
        # twn, = ax_twin.plot(pol_dom[key], kn[key], c='tab:orange')

        axs[i, j].spines["left"].set_edgecolor(axn.get_color())
        axs[i, j].tick_params(axis='y', colors=axn.get_color())

        ax_twin.spines["right"].set_edgecolor(twn.get_color())
        ax_twin.tick_params(axis='y', colors=twn.get_color())

        if i == 0:
            axs[i, j].set_title(conID+r' $\left(\mathcal{{K}} / \mathcal{{K}}^* = {0:0.2f}\right)$'.format(kappa[key]))
        if i+1 == len(ky_tags):
            axs[i, j].set_xlabel(r'$\theta/\pi$')

        if j == 0:
            phi_label = r'$|\phi|^2_{{k_y = {}}}$'.format(ky_tag.split('_')[1].replace('p', '.'))
            axs[i, j].set_ylabel(phi_label, color=axn.get_color())
        elif j+1 == len(conIDs):
            ax_twin.set_ylabel(r'B (T)', color=twn.get_color())
            # ax_twin.set_ylabel(r'$\kappa_n$', color=twn.get_color())

        kn_max = np.max(np.abs(kn[key]))
        # ax_twin.set_ylim(-kn_max, kn_max)

axs[0, 0].set_ylim(0, 1.0)
axs[0, 0].set_xlim(-npol, npol)

save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'gene_eigendata_modB.png')
plt.savefig(save_path)
# plt.show()
