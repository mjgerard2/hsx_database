import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm


# Read in New Metric Data #
new_met_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'new_metric_data.h5')
with hf.File(new_met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

is_nan = np.isnan(met_data[:, 3])
not_nan = ~is_nan
met_data = met_data[not_nan]

pos_idx = np.where(met_data[:, 6] < 0)[0]
# met_data = np.array(sorted(met_data[pos_idx], key=lambda x: x[4], reverse=True))

neg_idx = np.where(met_data[:, 6] > 0)[0]
met_data = np.array(sorted(met_data[neg_idx], key=lambda x: x[4], reverse=True))

# met_data = np.array(sorted(met_data, key=lambda x: x[4], reverse=True))

# Read in New Selected Metric Data #
sub_met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'new_metric_data_scaled.h5')
with hf.File(sub_met_path, 'r') as hf_:
    sub_data = hf_['metric data'][()]

sub_data = np.array(sorted(sub_data, key=lambda x: x[4], reverse=True))

# Generate Plot #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)
mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 1

# Plotting Axis #
fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))

smap = ax.scatter(met_data[:, 6], met_data[:, 7], c=met_data[:, 3], s=1, norm=LogNorm(vmin=np.min(met_data[:, 3]), vmax=np.max(met_data[:, 3])), cmap='viridis_r')
# ax.scatter(sub_data[:, 6], sub_data[:, 7], c=sub_data[:, 3], edgecolor='k', marker='s', s=100, norm=LogNorm(vmin=np.min(met_data[:, 3]), vmax=np.max(met_data[:, 3])), cmap='viridis_r')
# ax.scatter([-1.], [-1.], c='k', edgecolor='w', marker='*', s=350, zorder=10)

# Axis Labels #
cbar = fig.colorbar(smap, ax=ax)
cbar.ax.set_ylabel(r'$\mathcal{Q} / \mathcal{Q}^*$')

ax.set_xlabel(r'$\mathcal{B}_{\kappa} / |\mathcal{B}_{\kappa}^*|$')
ax.set_ylabel(r'$\mathcal{C}_{n} / |\mathcal{C}_{n}^*|$')

ax.grid()

# Save/Show #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'metric_database_negative.png')
plt.savefig(save_path)
# plt.show()
