import os
import h5py as hf
import numpy as np


met_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'new_metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

is_nan = np.isnan(met_data[:, 3])
not_nan = ~is_nan
met_data = met_data[not_nan]

idx = np.where(met_data[:, 6] > 0)[0]
met_data = met_data[idx]

qhs_idx = np.argmin(met_data[:, 4])
print(met_data[qhs_idx])
