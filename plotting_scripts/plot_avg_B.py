import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd
from vmecTools.wout_files import wout_read as wr


# Import Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Get Average Field Strength #
u_dom = np.linspace(-np.pi, np.pi, 151)
v_dom = np.linspace(-np.pi, np.pi, 4*u_dom.shape[0])
ampKeys = ['Bmod', 'Jacobian']

data = np.empty((met_data.shape[0], 3))
for i, met in enumerate(met_data):
    print('({}|{})'.format(i+1, met_data.shape[0]))
    mainID = 'main_coil_{0:0.0f}'.format(met[0])
    setID = 'set_{0:0.0f}'.format(met[1])
    jobID = 'job_{0:0.0f}'.format(met[2])

    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID)
    wout = wr.readWout(wout_path)

    wout.transForm_3D(wout.s_grid, u_dom, v_dom, ampKeys)
    Bmod = wout.invFourAmps['Bmod']
    jacob = wout.invFourAmps['Jacobian']

    B_avg = -np.trapz(np.trapz(np.trapz(Bmod*jacob, u_dom, axis=2), v_dom, axis=1), wout.s_grid, axis=0) / wout.volume
    data[i] = [met[9], B_avg, wout.volume]

# Get QHS Average #
wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0')
wout = wr.readWout(wout_path)

wout.transForm_3D(wout.s_grid, u_dom, v_dom, ampKeys)
Bmod = wout.invFourAmps['Bmod']
jacob = wout.invFourAmps['Jacobian']

B_avg = -np.trapz(np.trapz(np.trapz(Bmod*jacob, u_dom, axis=2), v_dom, axis=1), wout.s_grid, axis=0) / wout.volume
data_qhs = np.array([1, B_avg, wout.volume])

# Construct Plot #
plot = pd.plot_define()
fig, axs = plt.subplots(1, 2, sharex=True, sharey=False, tight_layout=True, figsize=(14, 6))

ax1 = axs[0]
ax2 = axs[1]

ax1.scatter(data[:, 0], data[:, 1], facecolor='None', edgecolor='tab:red', s=150, marker='o')
ax1.scatter(data_qhs[0], data_qhs[1], facecolors='None', edgecolors='tab:red', s=750, marker='*', c='k', edgecolor='w')

ax2.scatter(data[:, 0], data[:, 2], facecolor='None', edgecolor='tab:red', s=150, marker='o')
ax2.scatter(data_qhs[0], data_qhs[2], facecolors='None', edgecolors='tab:red', s=750, marker='*', c='k', edgecolor='w')

ax1.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax1.set_ylabel(r'$\langle B \rangle \ (\mathrm{T})$')

ax2.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax2.set_ylabel(r'$V \ (\mathrm{m}^3)$')

ax1.grid()
ax2.grid()

# plt.show()
save_path = os.path.join(os.path.dirname(os.getcwd()), 'figures', 'avg_B_and_volume.png')
plt.savefig(save_path)
