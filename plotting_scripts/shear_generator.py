import os, sys
import h5py as hf
import numpy as np

import scipy.interpolate as spi

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

from vmecTools.wout_files import wout_read as wr


# Import Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Calculate geometry terms from VMEC output #
psi_dom = np.array([0.2, 0.4, 0.6, 0.8])**2
pol_dom = np.linspace(-np.pi, np.pi, 151, endpoint=False)
tor_dom = np.linspace(-np.pi, np.pi, pol_dom.shape[0]*4, endpoint=False)

shear = np.empty((met_data.shape[0], psi_dom.shape[0]))
base_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs')
for idx, met in enumerate(met_data):
    print('({0:0.0f}|{1:0.0f})'.format(idx+1, met_data.shape[0]))
    mainID = 'main_coil_{0:0.0f}'.format(met[0])
    setID = 'set_{0:0.0f}'.format(met[1])
    jobID = 'job_{0:0.0f}'.format(met[2])
    wout_path = os.path.join(base_path, mainID, setID, jobID)
    wout = wr.readWout(wout_path, space_derivs=True)

    wout.transForm_3D(wout.s_grid, pol_dom, tor_dom, ['Jacobian'])
    jacob = wout.invFourAmps['Jacobian']
    vol_dom = np.zeros(wout.ns)
    for ns in range(1, wout.ns):
        j_dom = np.abs(jacob[0:ns+1, :, :])
        s_dom = wout.s_grid[0:ns+1]
        vol_dom[ns] = np.trapz(np.trapz(np.trapz(j_dom, pol_dom, axis=2), tor_dom, axis=1), s_dom)

    di_ds = spi.interp1d(wout.s_grid, np.gradient(wout.iota(wout.s_grid), wout.s_grid))
    ds_dV = spi.interp1d(wout.s_grid, np.gradient(wout.s_grid, vol_dom))

    for psi_idx, psi_val in enumerate(psi_dom):
        shear[idx, psi_idx] = -4*np.pi*np.pi*di_ds(psi_val)*ds_dV(psi_val)

# Calculate geometry from VMEC output for QHS #
print('\nGet QHS')
wout_path = os.path.join(base_path, 'main_coil_0', 'set_1', 'job_0')
wout = wr.readWout(wout_path, space_derivs=True)

wout.transForm_3D(wout.s_grid, pol_dom, tor_dom, ['Jacobian'])
jacob = wout.invFourAmps['Jacobian']
vol_dom = np.zeros(wout.ns)
for ns in range(1, wout.ns):
    j_dom = np.abs(jacob[0:ns+1, :, :])
    s_dom = wout.s_grid[0:ns+1]
    vol_dom[ns] = np.trapz(np.trapz(np.trapz(j_dom, pol_dom, axis=2), tor_dom, axis=1), s_dom)

di_ds = spi.interp1d(wout.s_grid, np.gradient(wout.iota(wout.s_grid), wout.s_grid))
ds_dV = spi.interp1d(wout.s_grid, np.gradient(wout.s_grid, vol_dom))

shear_qhs = np.empty(psi_dom.shape[0])
for psi_idx, psi_val in enumerate(psi_dom):
    shear_qhs[psi_idx] = -4*np.pi*np.pi*di_ds(psi_val)*ds_dV(psi_val)

# Save shear data #
hf_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'shear.h5')
with hf.File(hf_path, 'w') as hf_:
    for mdx, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        hf_.create_dataset(conID, data=shear[mdx])
    hf_.create_dataset('QHS', data=shear_qhs)
