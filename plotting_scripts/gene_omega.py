import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt


# Import Metric Data #
metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Metric Normalization #
metric_norm = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_normalizations.h5')
with hf.File(metric_norm, 'r') as hf_:
    qhs_tems = hf_['TEM'][()]

# Read in Growth Rates #
# hf_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_1_x.h5')
hf_path = os.path.join('/home', 'michael', 'Desktop', 'omega_data_5_x.h5')
with hf.File(hf_path, 'r') as hf_file:
    tem_data = np.full((100, 7, 3), np.nan)
    for m, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        tem_data[m] = hf_file[conID][()]

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22

# Plotting Axis #
fig, axs = plt.subplots(1, 1, tight_layout=False, figsize=(9, 7))

# Plot Growth Rates #
axs.scatter(met_data[:, 9], tem_data[:, 0, 2], marker='v', s=150, facecolor='None', edgecolor='tab:blue', label=r'$k_y \rho_s = 0.1$')
axs.scatter(met_data[:, 9], tem_data[:, 1, 2], marker='o', s=150, facecolor='None', edgecolor='tab:orange', label=r'$k_y \rho_s = 0.4$')
axs.scatter(met_data[:, 9], tem_data[:, 2, 2], marker='s', s=150, facecolor='None', edgecolor='tab:green', label=r'$k_y \rho_s = 0.7$')
axs.scatter(met_data[:, 9], tem_data[:, 3, 2], marker='^', s=150, facecolor='None', edgecolor='tab:red', label=r'$k_y \rho_s = 1.0$')
"""
axs.scatter([1.], qhs_tems[0, 2], marker='*', s=500, c='k', edgecolor='tab:blue')
axs.scatter([1.], qhs_tems[1, 2], marker='*', s=500, c='k', edgecolor='tab:orange')
axs.scatter([1.], qhs_tems[2, 2], marker='*', s=500, c='k', edgecolor='tab:green')
axs.scatter([1.], qhs_tems[3, 2], marker='*', s=500, c='k', edgecolor='tab:red')
"""
# Axis Limits #
axs.set_xlim(0.955, 1.08)
# axs.set_ylim(-0.9, 0.1)

# Axis Labels #
axs.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
axs.set_ylabel(r'$\omega \ (c_s/a)$')

# Axis Grids #
axs.grid()

# Axis Legends #
box = axs.get_position()
axs.set_position([box.x0, box.y0, box.width * 0.8, box.height])
axs.legend(loc='upper left', bbox_to_anchor=(1., 1))

# Plot/Save Figures #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'gene_omega.png')
# plt.savefig(save_path)
plt.show()
