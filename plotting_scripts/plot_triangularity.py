import os
import h5py as hf
import numpy as np
import matplotlib.pyplot as plt

import sys
modDIRC = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(modDIRC)

import plot_define as pd


# Import Triangularity Data #
tri_file = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'triangularity_data_sign.h5')
with hf.File(tri_file, 'r') as hf_:
    tri_data = hf_['triangularity data'][()]

# Read in Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data_scaled.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Import Subset of Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_sub = hf_['metric data'][()]

for i, met in enumerate(met_sub):
    idx = np.argmin(np.sum(np.abs(met[0:3] - met_data[:, 0:3]), axis=1))
    met_sub[i] = met_data[idx]

# Plot Data #
plot = pd.plot_define()
fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))

ax.scatter(met_sub[:, 9], tri_data[0:100, 3], s=50, marker='o', c='None', edgecolor='k')

ax.set_xlabel('K')
ax.set_ylabel('T')

ax.grid()

plt.savefig('../figures/triangularity_sign.png')
