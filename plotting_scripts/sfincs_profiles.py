import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid


# Read in Profile Data #
prof_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'sfincs_data', '0-1-0', 'profiles')
with open(prof_path, 'r') as f:
    lines = f.readlines()
    prof_data = np.empty((len(lines)-1, 5))
    for ldx, line in enumerate(lines[1::]):
        line_split = line.strip().split()
        prof_data[ldx, 0] = float(line_split[0])
        prof_data[ldx, 1] = float(line_split[4])
        prof_data[ldx, 2] = float(line_split[5])
        prof_data[ldx, 3] = float(line_split[6])
        prof_data[ldx, 4] = float(line_split[7])

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 4

# Plotting Axis #
# fig, axs = plt.subplots(2, 1, sharex=True, sharey=False, tight_layout=True, figsize=(8, 6))
fig = plt.figure(figsize=(8, 6))
grid = ImageGrid(fig, 111,
                 nrows_ncols=(2, 1),
                 axes_pad=0.5,
                 share_all=False,
                 aspect=False)

ax1 = grid[0]
ax2 = grid[1]

D_roa = prof_data[-1, 0] - prof_data[0, 0]
D_ne = prof_data[0, 3] - prof_data[-1, 3]
D_Te = prof_data[0, 4] - prof_data[-1, 4]

asp = 0.33
ax1.set_aspect(asp*(D_roa/D_ne))
ax2.set_aspect(asp*(D_roa/D_Te))

# Plot Density #
ax1.plot(prof_data[:, 0], prof_data[:, 3], ls='-', c='tab:blue', label='electron')
ax1.plot(prof_data[:, 0], prof_data[:, 1], ls=':', c='tab:orange', label='ion')

# Plot Temperature #
ax2.plot(prof_data[:, 0], prof_data[:, 4], ls='-', c='tab:blue')
ax2.plot(prof_data[:, 0], prof_data[:, 2], ls=':', c='tab:orange')

# Axis Labels #
ax1.set_ylabel(r'$n \ (10^{20} \ m^{-3})$')
ax2.set_ylabel(r'$T \ (keV)$')
ax2.set_xlabel(r'$r/a$')

# Axis Limits #
ax1.set_xlim(prof_data[0, 0], prof_data[-1, 0])
ax2.set_xlim(prof_data[0, 0], prof_data[-1, 0])
ax1.set_ylim(0, 0.2)
ax2.set_ylim(0, 1.1)

# Axis Grids #
ax1.grid()
ax2.grid()

# Axis Legends #
ax1.legend(loc='lower left', frameon=False)

# Axis Text #
x_txt = 0.9
ax1.text(x_txt, 0.171, '(a)')
ax2.text(x_txt, 0.95, '(b)')

# Save/Show Figure #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'sfincs_profiles.png')
plt.savefig(save_path)
# plt.show()
