import os
import numpy as np

import sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd
import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.read_boozmn as rb


# Define Equilibrium Paths #
wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_HSX_main_opt0.nc')
booz_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'boozmn_wout_HSX_main_opt0.nc')

# Instantiate Boozer File Object #
wout = wr.readWout(os.path.dirname(wout_path))
booz = rb.readBooz(booz_path, wout_path, space_derivs=True)

iota = booz.iota(0.5)
di_ds = np.gradient(booz.iota(booz.s_booz), booz.s_booz)
diota = di_ds[np.argmin(np.abs(booz.s_booz - 0.5))]

# Define Computational Grid #
psi_dom = booz.s_booz
pol_dom = np.linspace(-np.pi, np.pi, 11)
tor_dom = np.linspace(-np.pi, np.pi, pol_dom.shape[0]*4)

# Compute Quantities on Grid #
ampKeys = ['R',
           'dR_ds', 'dR_du', 'dR_dv',
           'dZ_ds', 'dZ_du', 'dZ_dv']

booz.metric_tensor_2D_sSec(0.5, pol_dom, tor_dom)
wout.transForm_2D_sSec(0.5, pol_dom, tor_dom, ['Bmod', 'Jacobian'])

B = wout.invFourAmps['Bmod']
jacob = wout.invFourAmps['Jacobian']

g_11 = booz.gss
g_23 = booz.g_uv
g_33 = booz.g_vv

# Calculate Flux-Surface Average #
# integ = ((g_11/B)**2)*(g_23 - iota * g_33)*jacob
integ = g_11 * jacob
# print(integ.shape)

norm_inv = 1. / np.trapz(np.trapz(jacob, pol_dom, axis=1), tor_dom, axis=0)
avg = np.trapz(np.trapz(integ, pol_dom, axis=1), tor_dom, axis=0) * norm_inv

print(avg)
#print((iota - 1)*diota*avg)
