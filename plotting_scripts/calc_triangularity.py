import os
import numpy as np
import h5py as hf
import matplotlib.pyplot as plt

import sys
modDIRC = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(modDIRC)

import plot_define as pd
import vmecTools.wout_files.wout_read as wr


# Triangularity Data File #
tri_file = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'triangularity_data_sign.h5')
if not os.path.isfile(tri_file):
    with hf.File(tri_file, 'w') as hf_:
        hf_.create_dataset('triangularity data', data=np.empty((101, 4)))

# Import Subset of Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_sub = hf_['metric data'][()]

met_qhs = np.array([[0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1]])
met_sub = np.append(met_sub, met_qhs, axis=0)

# Calculate Triangularity #
nfp = 4
triangularity = np.empty((met_sub.shape[0], 4))
for met_idx, met in enumerate(met_sub):
    print('({0:0.0f}|{1:0.0f})'.format(met_idx+1, met_sub.shape[0]))
    mainID = 'main_coil_{0:0.0f}'.format(met[0])
    setID = 'set_{0:0.0f}'.format(met[1])
    jobID = 'job_{0:0.0f}'.format(met[2])
    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID)

    # Instantiate wout file #
    wout = wr.readWout(wout_path)

    # Define Computational Grid #
    u_dom = np.linspace(0, 2*np.pi, 100, endpoint=False)
    v_dom = np.linspace(0, 0.5*np.pi, u_dom.shape[0], endpoint=False)

    u_pts = u_dom.shape[0]
    v_pts = v_dom.shape[0]

    wout.transForm_2D_sSec(1, u_dom, v_dom, ['R', 'Z'])

    minor = wout.a_minor
    minor_inv = 1./minor

    R_grid = wout.invFourAmps['R']
    Z_grid = wout.invFourAmps['Z']

    # Calculate Triangularity #
    r_ma_vec = np.stack((R_grid[:, 0], Z_grid[:, 0]), axis=1)

    D_u = np.empty(v_pts)
    for v_idx, v_val in enumerate(v_dom):
        the = nfp * v_val
        mat = np.array([[np.cos(the), -np.sin(the)], [np.sin(the), np.cos(the)]])

        R_vec = np.stack((R_grid[v_idx, :], Z_grid[v_idx, :]), axis=1) - r_ma_vec[v_idx]
        r_vec = np.empty(R_vec.shape)
        for i in range(u_dom.shape[0]):
            r_vec[i] = np.dot(mat, R_vec[i])

        R_geo = 0.5 * (np.max(r_vec[:, 0]) + np.min(r_vec[:, 0]))
        D_u[v_idx] = (R_geo - r_vec[np.argmax(r_vec[:, 1]), 0]) * minor_inv

    D_u_mean = np.trapz(D_u, v_dom) / (v_dom[-1] - v_dom[0])
    with hf.File(tri_file, 'a') as hf_:
        tri_data = hf_['triangularity data']
        tri_data[met_idx] = np.append(met[0:3], D_u_mean)

"""
# Plot Data #
plot = pd.plot_define()
fig, ax = plt.subplots(1, 1, sharex=False, tight_layout=True, figsize=(8, 6))

# D_u_plot = np.trapz(D_u*jacob, v_dom, axis=1) * vol_inv
# D_l_plot = np.trapz(D_l*jacob, v_dom, axis=1) * vol_inv

ax.plot(v_dom/np.pi, D_u, c='k', label=r'$\delta_u$')
# ax.plot(s_dom, D_l_plot, c='tab:red', label=r'$\delta_l$')

ax.set_xlabel(r'$\zeta / np.pi$')
ax.set_ylabel(r'$\delta$')

ax.legend()
ax.grid()

plt.show()
"""
