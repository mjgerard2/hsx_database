import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt


aLn = 2
aLT = 2

psiN = 0.039418
psiHat = 0.5

xi_T = -aLT / (2*psiN*np.sqrt(psiHat))
xi_n = -aLn / (2*psiN*np.sqrt(psiHat))

sigma = 0.1
omega = -xi_n

well_IDs = np.arange(6, dtype=int)
well_dirc = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs')
# well_dirc = os.path.join('/home', 'michael', 'HSX_Database_Overflow')
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')

# Extract Metric Data #
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

met_min = 0.995 * np.min(met_data[:, 9])
met_max = 1.005 * np.max(met_data[:, 9])

# Read in Growth Rates #
hf_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_1_x.h5')
with hf.File(hf_path, 'r') as hf_file:
    tem_data = np.full((100, 7, 3), np.nan)
    for m, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        tem_data[m] = hf_file[conID][()]

# Metric Normalization #
metric_norm = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_normalizations.h5')
with hf.File(metric_norm, 'r') as hf_:
    tem_qhs = hf_['TEM'][()]

# Append Metric Data with QHS #
qhs_data = np.array([0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1])
met_data = np.append(met_data, qhs_data).reshape(met_data.shape[0]+1, met_data.shape[1])

# Extract Well Data #
omega_avg = np.empty((well_IDs.shape[0], met_data.shape[0]-1))
well_data = np.empty((well_IDs.shape[0], met_data.shape[0]-1))
for mdx, met in enumerate(met_data[0:-1]):
    print('({0:0.0f}|{1:0.0f})'.format(mdx+1, met_data.shape[0]-1))
    mainID = 'main_coil_{0:0.0f}'.format(met[0])
    setID = 'set_{0:0.0f}'.format(met[1])
    jobID = 'job_{0:0.0f}'.format(met[2])
    conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])

    well_path = os.path.join(well_dirc, mainID, setID, jobID, 'trapping_wells_res1024.h5')
    with hf.File(well_path, 'r') as hf_:
        for well_ID in well_IDs:
            well_key = 'well {0:0.0f}'.format(well_ID)
            theta = hf_[well_key+'/theta_bnc'][()]
            omegaD = hf_[well_key+'/omegaD'][()]
            p_wght = hf_[well_key+'/p_wght'][()]

            omega_avg[well_ID, mdx] = np.trapz(omegaD*p_wght, theta) / np.trapz(p_wght, theta)
            # well_data[well_ID, mdx] = np.trapz(F_theta*gyy_inv*p_wght, theta)

omega_qhs = np.empty(well_IDs.shape[0])
well_qhs = np.empty(well_IDs.shape[0])

well_path = os.path.join(well_dirc, 'main_coil_0', 'set_1', 'job_0', 'trapping_wells_res1024.h5')
with hf.File(well_path, 'r') as hf_:
    for well_ID in well_IDs:
        well_key = 'well {0:0.0f}'.format(well_ID)
        theta = hf_[well_key+'/theta_bnc'][()]
        omegaD = hf_[well_key+'/omegaD'][()]
        p_wght = hf_[well_key+'/p_wght'][()]

        omega_qhs[well_ID] = np.trapz(omegaD*p_wght, theta) / np.trapz(p_wght, theta)
        # well_qhs[well_ID] = np.trapz(F_theta*gyy_inv*p_wght, theta)

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Plotting Axis #
fig, axs = plt.subplots(2, 3, sharex=True, sharey=False, tight_layout=False, figsize=(16, 8))

ax11 = axs[0, 0]
ax12 = axs[0, 1]
ax13 = axs[0, 2]

ax21 = axs[1, 0]
ax22 = axs[1, 1]
ax23 = axs[1, 2]

# Plot \Delta \theta = 2\pi Wells #
omega_max = np.max(np.abs(omega_avg))
colors = plt.cm.viridis(np.linspace(0, 1, 3))
cmap = 'plasma'

tem_idx = 1
tem_min = np.min(tem_data[:, tem_idx, 1])
tem_max = np.max(tem_data[:, tem_idx, 1])

im = ax11.scatter(met_data[0:-1, 9], omega_avg[0, :], s=150, c=tem_data[:, tem_idx, 1], edgecolor='k', marker='o', vmin=tem_min, vmax=tem_max, cmap=cmap)
ax11.scatter([met_data[-1, 9]], [omega_qhs[0]], s=500, c=[tem_qhs[tem_idx, 1]], edgecolor='w', marker='*', vmin=tem_min, vmax=tem_max, cmap=cmap)

ax12.scatter(met_data[0:-1, 9], omega_avg[1, :], s=150, c=tem_data[:, tem_idx, 1], edgecolor='k', marker='o', vmin=tem_min, vmax=tem_max, cmap=cmap)
ax12.scatter([met_data[-1, 9]], [omega_qhs[1]], s=500, c=[tem_qhs[tem_idx, 1]], edgecolor='w', marker='*', vmin=tem_min, vmax=tem_max, cmap=cmap)

ax13.scatter(met_data[0:-1, 9], omega_avg[2, :], s=150, c=tem_data[:, tem_idx, 1], edgecolor='k', marker='o', vmin=tem_min, vmax=tem_max, cmap=cmap)
ax13.scatter([met_data[-1, 9]], [omega_qhs[2]], s=500, c=[tem_qhs[tem_idx, 1]], edgecolor='w', marker='*', vmin=tem_min, vmax=tem_max, cmap=cmap)

ax21.scatter(met_data[0:-1, 9], omega_avg[3, :], s=150, c=tem_data[:, tem_idx, 1], edgecolor='k', marker='o', vmin=tem_min, vmax=tem_max, cmap=cmap)
ax21.scatter([met_data[-1, 9]], [omega_qhs[3]], s=500, c=[tem_qhs[tem_idx, 1]], edgecolor='w', marker='*', vmin=tem_min, vmax=tem_max, cmap=cmap)

ax22.scatter(met_data[0:-1, 9], omega_avg[4, :], s=150, c=tem_data[:, tem_idx, 1], edgecolor='k', marker='o', vmin=tem_min, vmax=tem_max, cmap=cmap)
ax22.scatter([met_data[-1, 9]], [omega_qhs[4]], s=500, c=[tem_qhs[tem_idx, 1]], edgecolor='w', marker='*', vmin=tem_min, vmax=tem_max, cmap=cmap)

ax23.scatter(met_data[0:-1, 9], omega_avg[5, :], s=150, c=tem_data[:, tem_idx, 1], edgecolor='k', marker='o', vmin=tem_min, vmax=tem_max, cmap=cmap)
ax23.scatter([met_data[-1, 9]], [omega_qhs[5]], s=500, c=[tem_qhs[tem_idx, 1]], edgecolor='w', marker='*', vmin=tem_min, vmax=tem_max, cmap=cmap)

# Axis Colorbar #
fig.subplots_adjust(right=0.85)
cbar_ax = fig.add_axes([0.87, 0.15, 0.03, 0.7])
cbar = fig.colorbar(im, cax=cbar_ax)
cbar.ax.set_ylabel(r'$\gamma \ (c_s/a) \ \left[k_y = 0.4\right]$')

# Axis Limits #
ax11.set_xlim(met_min, met_max)

# Axis Labels #
ax11.set_ylabel(r'$\langle \overline{\omega}_{d\mathrm{e}} \rangle \ (k_{\alpha}K/\mathrm{e})$')
ax21.set_ylabel(r'$\langle \overline{\omega}_{d\mathrm{e}} \rangle \ (k_{\alpha}K/\mathrm{e})$')

ax21.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax22.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax23.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

ax11.set_title(r'$i = 0$')
ax12.set_title(r'$i = 1$')
ax13.set_title(r'$i = 2$')

ax21.set_title(r'$i = 3$')
ax22.set_title(r'$i = 4$')
ax23.set_title(r'$i = 5$')

# Axis Text #
"""
x_txt = 0.963
ax11.text(x_txt, -.31, '(a)')
ax12.text(x_txt, 2.4, '(b)')
ax13.text(x_txt, -.5, '(c)')
ax21.text(x_txt, -1.22, '(d)')
ax22.text(x_txt, 2.25, '(e)')
ax23.text(x_txt, -1.6, '(f)')
"""
# Axis Grids #
ax11.grid()
ax12.grid()
ax13.grid()

ax21.grid()
ax22.grid()
ax23.grid()

# Axis Show/Save #
save_path = os.path.join('/home', 'michael', 'Desktop', 'Prelim_Defense', 'figures', 'drifts_across_wells.png')
plt.savefig(save_path)
# plt.show()
