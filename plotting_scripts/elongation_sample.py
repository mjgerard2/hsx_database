import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.gridspec as gsp
import matplotlib.pyplot as plt

from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import ImageGrid

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

from vmecTools.wout_files import wout_read as wr
from vmecTools.wout_files import curveB_tools as curB


def get_extrema(data):
    data_min = np.inf
    data_max = -np.inf
    for key in data:
        if np.min(data[key]) < data_min:
            data_min = np.min(data[key])
        if np.max(data[key]) > data_max:
            data_max = np.max(data[key])
    return data_min, data_max


tor_angle = 0.0*np.pi

# Import Metric Data #
metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Import Triangularity Data #
triang_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'triangularity_data.h5')
with hf.File(triang_path, 'r') as hf_:
    tri_data = hf_['triangularity data'][()]

# Identify Configurations to Plot #
idx_min = np.argmin(met_data[:, 9])
idx_max = np.argmax(met_data[:, 9])

kappa_min = met_data[idx_min, 9]
kappa_max = met_data[idx_max, 9]

conID_min = '-'.join(['{0:0.0f}'.format(iD) for iD in met_data[idx_min, 0:3]])
conID_max = '-'.join(['{0:0.0f}'.format(iD) for iD in met_data[idx_max, 0:3]])

conIDs = [conID_min, '0-1-0', conID_max]
kappa_values = np.array([kappa_min, 1., kappa_max])

# Get Surface Data #
psi_dom = np.linspace(.2, 1, 5)**2
pol_dom = np.linspace(-np.pi, np.pi, 501, endpoint=False)
tor_dom = np.linspace(0, .25*np.pi, 3)

flux_surf = np.empty((len(conIDs), psi_dom.shape[0], tor_dom.shape[0], pol_dom.shape[0], 2))
for cdx, conID in enumerate(conIDs):
    print(conID+' ({0:0.0f}|{1:0.0f})'.format(cdx+1, len(conIDs)))
    conArr = np.array([float(iD) for iD in conID.split('-')])
    mainID = 'main_coil_{0:0.0f}'.format(conArr[0])
    setID = 'set_{0:0.0f}'.format(conArr[1])
    jobID = 'job_{0:0.0f}'.format(conArr[2])
    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID)

    wout = wr.readWout(wout_path, space_derivs=False, field_derivs=False)
    ampKeys = ['R', 'Z']

    # wout.transForm_2D_vSec(wout.s_grid[beg_idx::], pol_dom, tor_angle, ampKeys)
    wout.transForm_3D(psi_dom, pol_dom, tor_dom, ampKeys)
    for psi_idx, psi_val in enumerate(psi_dom):
        flux_surf[cdx, psi_idx] = np.stack((wout.invFourAmps['R'][psi_idx], wout.invFourAmps['Z'][psi_idx]), axis=2)

# Plot Axis #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 14}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['lines.linewidth'] = 1

# Plotting Axis #
fig = plt.figure(figsize=(12, 4))
axs = ImageGrid(fig, 111,
                nrows_ncols=(1, 3),
                axes_pad=0.25,
                aspect=True,
                share_all=True)

for cdx, conID in enumerate(conIDs):
    for pdx in range(psi_dom.shape[0]):
        for tdx in range(tor_dom.shape[0]):
            axs[cdx].plot(flux_surf[cdx, pdx, tdx, :, 0], flux_surf[cdx, pdx, tdx, :, 1], c='k')

    # Axis Titles #
    axs[cdx].set_title(r'$\mathcal{{K}} / \mathcal{{K}}^* = {0:0.2f}$'.format(kappa_values[cdx]))

# Axis Labels #
axs[0].set_ylabel(r'$Z$')
axs[0].set_xlabel(r'$R$')
axs[1].set_xlabel(r'$R$')
axs[2].set_xlabel(r'$R$')

# Axis Limits #
# axs[0].set_xlim(1.25, 1.63)
# axs[0].set_ylim(-.3, .3)

# Axis Text #
x_txt, y_txt = 0.855, -0.28
axs[0].text(x_txt, y_txt, '(a)')
axs[1].text(x_txt, y_txt, '(b)')
axs[2].text(x_txt, y_txt, '(c)')

# Adjust Figure Layout #
plt.gcf().subplots_adjust(right=0.88)

# Save/Show Figure #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'elongation_sample.png')
# plt.savefig(save_path)
plt.show()
