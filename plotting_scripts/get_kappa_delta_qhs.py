import os
import numpy as np

import sys
modDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(modDirc)
import vmecTools.wout_files.wout_read as wr


# Import QHS Wout File #
wout_dirc = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0')
wout = wr.readWout(wout_dirc)
