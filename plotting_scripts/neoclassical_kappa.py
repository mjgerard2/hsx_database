import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm


# Import Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

met_data = met_data[met_data[:, 3] <= 10]
met_data = np.array(sorted(met_data, key=lambda x: x[3], reverse=True))

# Import Subset of Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_sub = hf_['metric data'][()]

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 24}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 28

# Plotting Axis #
fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))

# Plot Metrics #
smap = ax.scatter(met_data[:, 9], met_data[:, 4], c=met_data[:, 3], s=1, norm=LogNorm(vmin=np.min(met_data[:, 3]), vmax=10), cmap='viridis_r')
ax.scatter(met_sub[:, 9], met_sub[:, 4], c=met_sub[:, 3], edgecolor='tab:red', s=100, marker='s', norm=LogNorm(vmin=np.min(met_data[:, 3]), vmax=10), cmap='viridis_r')
ax.scatter([1.], [1.], c='k', edgecolor='w', marker='*', s=750)

# Axis Labels #
ax.set_ylabel(r'$\mathcal{Q} / \mathcal{Q}^*$')
ax.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

cbar = fig.colorbar(smap, ax=ax)
cbar.ax.set_ylabel(r'$\mathcal{E} / \mathcal{E}^*$')

# Axis Limits #
ax.set_xlim(0.95, 1.08)

# Axis Grids #
ax.grid()

# Save/Show #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'neoclassical_kappa.png')
plt.savefig(save_path)
# plt.show()
