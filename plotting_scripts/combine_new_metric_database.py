import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm


# Read in Old Metric Data #
old_met_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data_scaled.h5')
with hf.File(old_met_path, 'r') as hf_:
    old_met_data = hf_['metric data'][()]

# Read in Metric Norms #
norm_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'new_metric_norms.h5')
with hf.File(norm_path, 'r') as hf_:
    F_kappa = hf_['F kappa'][()]
    G_psi = hf_['G psi'][()]
    FG_both = hf_['FG both'][()]
met_norms = np.array([F_kappa, G_psi, FG_both])

# Read in New Metric Data #
data_dirc = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'chtc_metric_output')
main_names = [f.name for f in os.scandir(data_dirc)]

new_met_data = np.full(old_met_data.shape, np.nan)
for mdx, main_name in enumerate(main_names):
    print('({0:0.0f}|{1:0.0f})'.format(mdx+1, len(main_names)))
    main_id = main_name.split('_')[-1].split('.')[0]
    main_idx = np.where(old_met_data[:, 0] == float(main_id))[0]

    main_path = os.path.join(data_dirc, main_name)
    with hf.File(main_path, 'r') as hf_:
        main_data = hf_['metric data'][()]

    for main in main_data:
        job_id = main[0]
        job_idx_set = np.where(old_met_data[main_idx, 2] == job_id)[0]
        if len(job_idx_set) == 0:
            new_id = '\t{0}-{1}-{2:0.0f}'.format(main_id, 1, job_id)
            print(new_id+' is missing from old data set.')
        elif len(job_idx_set) > 1:
            new_id = '\t{0}-{1}-{2:0.0f}'.format(main_id, 1, job_id)
            print(new_id+' appears multiple times in old data set.')
        else:
            idx = main_idx[job_idx_set[0]]
            old_met_data[idx, 6] = main[1] / met_norms[0]
            old_met_data[idx, 7] = main[2] / met_norms[1]
            new_met_data[idx] = old_met_data[idx]

# Save New Dataset #
new_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'new_metric_data.h5')
with hf.File(new_path, 'w') as hf_:
    hf_.create_dataset('metric data', data=new_met_data)
