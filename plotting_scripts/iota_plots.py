import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

from vmecTools.wout_files import wout_read as wr


# Import Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data_scaled.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Import iota data #
hf_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'iota.h5')
with hf.File(hf_path, 'r') as hf_:
    iota_qhs = hf_['QHS'][()]
    iota = np.empty((met_data.shape[0], iota_qhs.shape[0]))
    for mdx, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        iota[mdx] = hf_[conID][()]

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 22}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 26

# Plotting Axis #
fig = plt.figure(figsize=(10, 8))
axs = ImageGrid(fig, 111,
                nrows_ncols=(2, 2),
                axes_pad=0.5,
                share_all=True,
                aspect=False)

ax1 = axs[0]
ax2 = axs[1]
ax3 = axs[2]
ax4 = axs[3]

# Plot g_ss Data #
ax1.scatter(met_data[:, 9], iota[:, 0], marker='o', s=150, facecolor='None', edgecolor='tab:red')
ax2.scatter(met_data[:, 9], iota[:, 1], marker='o', s=150, facecolor='None', edgecolor='tab:red')
ax3.scatter(met_data[:, 9], iota[:, 2], marker='o', s=150, facecolor='None', edgecolor='tab:red')
ax4.scatter(met_data[:, 9], iota[:, 3], marker='o', s=150, facecolor='None', edgecolor='tab:red')

ax1.scatter([1.], iota_qhs[0], marker='*', s=750, c='k', edgecolor='w')
ax2.scatter([1.], iota_qhs[1], marker='*', s=750, c='k', edgecolor='w')
ax3.scatter([1.], iota_qhs[2], marker='*', s=750, c='k', edgecolor='w')
ax4.scatter([1.], iota_qhs[3], marker='*', s=750, c='k', edgecolor='w')

x_min, x_max = ax1.get_xlim()

ax1.plot([x_min, x_max], [1.]*2, c='k', ls='--')
ax1.plot([x_min, x_max], [8./7]*2, c='k', ls='-.')
# ax1.plot([x_min, x_max], [12./10]*2, c='k', ls=':')
# ax1.plot([x_min, x_max], [16./13]*2, c='k', ls=':')

ax2.plot([x_min, x_max], [1.]*2, c='k', ls='--')
ax2.plot([x_min, x_max], [8./7]*2, c='k', ls='-.')
# ax2.plot([x_min, x_max], [12./10]*2, c='k', ls=':')
# ax2.plot([x_min, x_max], [16./13]*2, c='k', ls=':')

ax3.plot([x_min, x_max], [1.]*2, c='k', ls='--')
ax3.plot([x_min, x_max], [8./7]*2, c='k', ls='-.')
# ax3.plot([x_min, x_max], [12./10]*2, c='k', ls=':')
# ax3.plot([x_min, x_max], [16./13]*2, c='k', ls=':')

ax4.plot([x_min, x_max], [1.]*2, c='k', ls='--')
ax4.plot([x_min, x_max], [8./7]*2, c='k', ls='-.')
# ax4.plot([x_min, x_max], [12./10]*2, c='k', ls=':')
# ax4.plot([x_min, x_max], [16./13]*2, c='k', ls=':')

# Axis Limits #
ax1.set_xlim(x_min, x_max)
ax1.set_ylim(0.94, 1.25)
ax2.set_ylim(0.94, 1.25)
ax3.set_ylim(0.94, 1.25)
ax4.set_ylim(0.94, 1.25)

# Axis Labels #
ax1.set_ylabel(r'$\iota/2\pi$')
ax3.set_ylabel(r'$\iota/2\pi$')

ax3.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax4.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

# Axis titles #
ax1.set_title(r'$r/a = 0.2$')
ax2.set_title(r'$r/a = 0.4$')
ax3.set_title(r'$r/a = 0.6$')
ax4.set_title(r'$r/a = 0.8$')

# Axis Text #
x1_txt, y1_txt = 1.04, 1.005
x2_txt, y2_txt = 0.98, (8./7)+.005
y3_txt = 1.205
y4_txt = (16./13)+.005

ax1.text(x1_txt, y1_txt, '4/4', fontsize=22)
ax1.text(x2_txt, y2_txt, '8/7', fontsize=22)
# ax1.text(x2_txt, y3_txt, '12/10', fontsize=22)
# ax1.text(x2_txt, y4_txt, '16/13', fontsize=22)
# ax1.text(x2_txt, (4./3)+.005, '4/3', fontsize=22)

ax2.text(x1_txt, y1_txt, '4/4', fontsize=22)
ax2.text(x2_txt, y2_txt, '8/7', fontsize=22)
# ax2.text(x2_txt, y3_txt, '12/10', fontsize=22)
# ax2.text(x2_txt, y4_txt, '16/13', fontsize=22)

ax3.text(x1_txt, y1_txt, '4/4', fontsize=22)
ax3.text(x2_txt, y2_txt, '8/7', fontsize=22)
# ax3.text(x2_txt, y3_txt, '12/10', fontsize=22)
# ax3.text(x2_txt, y4_txt, '16/13', fontsize=22)

ax4.text(x1_txt, y1_txt, '4/4', fontsize=22)
ax4.text(x2_txt, y2_txt, '8/7', fontsize=22)
# ax4.text(x2_txt, y3_txt, '12/10', fontsize=22)
# ax4.text(x2_txt, y4_txt, '16/13', fontsize=22)
"""
x_txt, y_txt = 1.065, 0.95
ax1.text(x_txt, y_txt, '(a)')
ax2.text(x_txt, y_txt, '(b)')
ax3.text(x_txt, y_txt, '(c)')
ax4.text(x_txt, y_txt, '(d)')
"""
# Axis Grids #
ax1.grid()
ax2.grid()
ax3.grid()
ax4.grid()

# Plot/Save Figures #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'iota.png')
plt.savefig(save_path)
# plt.show()
