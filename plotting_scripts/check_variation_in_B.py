import os
import h5py as hf
import numpy as np

import sys
modDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(modDirc)

import vmecTools.wout_files.wout_read as wr


# Import metric data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'new_metric_data_scaled.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Calculate Varaitions #
B_mean = np.empty(met_data.shape[0])
volume = np.empty(met_data.shape[0])

npts = 151
u_dom = np.linspace(0, 2*np.pi, npts)
v_dom = np.linspace(0, 0.5*np.pi, npts)

base_dirc = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs')
for mdx, met in enumerate(met_data):
    print('({0:0.0f}|{1:0.0f})'.format(mdx+1, met_data.shape[0]))

    main_id = 'main_coil_{}'.format(int(met[0]))
    set_id = 'set_{}'.format(int(met[1]))
    job_id = 'job_{}'.format(int(met[2]))

    wout_dirc = os.path.join(base_dirc, main_id, set_id, job_id)
    wout = wr.readWout(wout_dirc)
    volume[mdx] = wout.volume

    wout.transForm_3D(wout.s_grid, u_dom, v_dom, ['Bmod', 'Jacobian'])
    B_jacob = wout.invFourAmps['Bmod'] * np.abs(wout.invFourAmps['Jacobian'])
    vol_norm = (1./wout.volume) * (2*np.pi/(v_dom[-1] - v_dom[0]))
    B_mean[mdx] = vol_norm * np.trapz(np.trapz(np.trapz(B_jacob, u_dom, axis=2), v_dom, axis=1), wout.s_grid)

# Print Extrema #
Bmin, Bmax = np.min(B_mean), np.max(B_mean)
Vmin, Vmax = np.min(volume), np.max(volume)

print('<B>: {} --> {}'.format(Bmin, Bmax))
print('V: {} --> {}'.format(Vmin, Vmax))
