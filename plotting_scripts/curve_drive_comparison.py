import os
import h5py as hf
import numpy as np
import matplotlib.pyplot as plt

import sys
modDIRC = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(modDIRC)

import plot_define as pd
import gistTools.gist_reader as gr


# Import Metric Data #
metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'new_metric_data_scaled.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Identify Configurations to Plot #
idx_min = np.argmin(met_data[:, 9])
idx_max = np.argmax(met_data[:, 9])

kappa_min = met_data[idx_min, 9]
kappa_max = met_data[idx_max, 9]

conID_min = '-'.join(['{0:0.0f}'.format(iD) for iD in met_data[idx_min, 0:3]])
conID_max = '-'.join(['{0:0.0f}'.format(iD) for iD in met_data[idx_max, 0:3]])

conIDs = [conID_min, conID_max]
kappa_values = np.array([kappa_min, kappa_max])

# Plot Results #
plot = pd.plot_define(lineWidth=2)
fig, axs = plt.subplots(2, 1, sharex=True, tight_layout=True, figsize=(16, 8))
ax1 = axs[0]
ax2 = ax1.twinx()

ax3 = axs[1]
ax4 = ax3.twinx()

ax1.spines['left'].set_color('tab:blue')
ax1.tick_params(axis='y', colors='tab:blue')

ax2.spines['right'].set_color('tab:orange')
ax2.tick_params(axis='y', colors='tab:orange')

ax3.spines['left'].set_color('tab:blue')
ax3.tick_params(axis='y', colors='tab:blue')

ax4.spines['right'].set_color('tab:orange')
ax4.tick_params(axis='y', colors='tab:orange')

ax1.set_ylabel(r'$B \ (T)$', color='tab:blue')
ax3.set_ylabel(r'$B \ (T)$', color='tab:blue')
ax3.set_xlabel(r'$\theta / \pi$')

pol_lim = 4*np.pi
ax1.set_xlim(-pol_lim/np.pi, pol_lim/np.pi)

# Gist path #
axis = [ax1, ax2, ax3, ax4]
for cdx, conID in enumerate(conIDs):
    main_id = 'main_coil_{}'.format(conID.split('-')[0])
    set_id = 'set_{}'.format(conID.split('-')[1])
    job_id = 'job_{}'.format(conID.split('-')[2])

    gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'gist_HSX_{}_s0p5_res1024_npol8_alpha0p00.dat'.format(conID))
    gist = gr.read_gist(gist_path)

    idx_above = np.where(gist.pol_dom >= -pol_lim)[0]
    idx_below = np.where(gist.pol_dom <= pol_lim)[0]
    idx = np.intersect1d(idx_above, idx_below)

    pol_norm = gist.pol_dom[idx] / np.pi

    B = gist.data['modB'][idx]
    sqrt_gxx = np.sqrt(gist.data['gxx'][idx])
    Kn = gist.data['Kn'][idx]
    Kg = gist.data['Kg'][idx]
    D = gist.data['D'][idx]

    Kn_term = Kn/sqrt_gxx
    curve_drive = (Kn/sqrt_gxx) + (D*Kg*(sqrt_gxx/B))

    axis[cdx*2].plot(pol_norm, B, c='tab:blue')
    axis[(cdx*2)+1].plot(pol_norm, curve_drive, c='tab:orange', label=r'$C_d$')
    axis[(cdx*2)+1].plot(pol_norm, Kn_term, c='tab:orange', ls='--', label=r'$\kappa_n / |\nabla \psi|$')

    Cd_max = 1.5*np.max(np.abs(np.r_[curve_drive, Kn_term]))
    axis[(cdx*2)+1].set_ylim(-Cd_max, Cd_max)

ax2.legend(loc='upper right')
ax2.grid()
ax4.grid()

# plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'curvature_drive_comparison.png')
plt.savefig(save_path)
