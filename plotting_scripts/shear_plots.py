import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

from vmecTools.wout_files import wout_read as wr


# Import Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Import shear data #
hf_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'shear.h5')
with hf.File(hf_path, 'r') as hf_:
    shear_qhs = hf_['QHS'][()]
    shear = np.empty((met_data.shape[0], shear_qhs.shape[0]))
    for mdx, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        shear[mdx] = hf_[conID][()]

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22

# Plotting Axis #
fig, axs = plt.subplots(2, 2, sharex=True, sharey=False, tight_layout=True, figsize=(10, 8))

ax1 = axs[0, 0]
ax2 = axs[0, 1]
ax3 = axs[1, 0]
ax4 = axs[1, 1]

# Plot g_ss Data #
ax1.scatter(met_data[:, 9], shear[:, 0], marker='v', s=150, facecolor='None', edgecolor='tab:blue', label=r'$r/a = 0.2$')
ax2.scatter(met_data[:, 9], shear[:, 1], marker='o', s=150, facecolor='None', edgecolor='tab:orange', label=r'$r/a = 0.4$')
ax3.scatter(met_data[:, 9], shear[:, 2], marker='s', s=150, facecolor='None', edgecolor='tab:green', label=r'$r/a = 0.6$')
ax4.scatter(met_data[:, 9], shear[:, 3], marker='^', s=150, facecolor='None', edgecolor='tab:red', label=r'$r/a = 0.8$')

ax1.scatter([1.], shear_qhs[0], marker='*', s=500, c='k', edgecolor='w')
ax2.scatter([1.], shear_qhs[1], marker='*', s=500, c='k', edgecolor='w')
ax3.scatter([1.], shear_qhs[2], marker='*', s=500, c='k', edgecolor='w')
ax4.scatter([1.], shear_qhs[3], marker='*', s=500, c='k', edgecolor='w')

# Axis Limits #
ax1.set_xlim(0.955, 1.08)

# Axis Labels #
ax1.set_ylabel(r'$\langle s \rangle$')
ax3.set_ylabel(r'$\langle s \rangle$')

ax3.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax4.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

# Axis Grids #
ax1.grid()
ax2.grid()
ax3.grid()
ax4.grid()

# Axis Legends #
ax1.legend(loc='lower right')
ax2.legend(loc='lower right')
ax3.legend(loc='upper right')
ax4.legend(loc='upper right')

# Axis Text #
x_txt = 0.96
ax1.text(x_txt, 15, '(a)')
ax2.text(x_txt, 0.75, '(b)')
ax3.text(x_txt, -10.1, '(c)')
ax4.text(x_txt, -17.2, '(d)')

# Plot/Save Figures #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'shear.png')
plt.savefig(save_path)
