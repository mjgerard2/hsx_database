import os
import h5py as hf
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import ImageGrid

import sys
modDIRC = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(modDIRC)

import plot_define as pd
import vmecTools.wout_files.wout_read as wr


# Import Triangularity Data #
tri_file = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'triangularity_data.h5')
with hf.File(tri_file, 'r') as hf_:
    tri_data = hf_['triangularity data'][()]

# Import Subset of Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'new_metric_data_scaled.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

met_qhs = np.array([[0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]])
met_data = np.append(met_data, met_qhs, axis=0)

# Normalize Data #
tri_qhs = tri_data[-1, 3]
tri_data = tri_data[0:100, 3] / tri_qhs
kap_data = met_data[0:100, 9]
met_data = met_data[0:100]

# Plot Data #
plot = pd.plot_define(fontSize=24, labelSize=28)
fig = plt.figure(1, figsize=(11, 9))
grid = ImageGrid(fig, 111,
                 nrows_ncols=(2, 2),
                 axes_pad=0.25,
                 aspect=False,
                 cbar_mode="single",
                 cbar_location="right",
                 cbar_size=0.35)

ax11 = grid[0]
ax12 = grid[1]
ax21 = grid[2]
ax22 = grid[3]

asp_base = 1.
asp11 = (np.max(met_data[:, 6]) - np.min(met_data[:, 6])) / (np.max(kap_data) - np.min(kap_data))
ax11.set_aspect(asp_base*asp11)
asp21 = (np.max(met_data[:, 6]) - np.min(met_data[:, 6])) / (np.max(tri_data) - np.min(tri_data))
ax21.set_aspect(asp_base*asp21)

asp12 = (np.max(met_data[:, 7]) - np.min(met_data[:, 7])) / (np.max(kap_data) - np.min(kap_data))
ax12.set_aspect(asp_base*asp12)
asp22 = (np.max(met_data[:, 7]) - np.min(met_data[:, 7])) / (np.max(tri_data) - np.min(tri_data))
ax22.set_aspect(asp_base*asp22)

# Plot Geometry Data #
smap = ax11.scatter(met_data[:, 6], kap_data, c=met_data[:, 4], edgecolor='k', s=150, marker='o', vmin=np.nanmin(met_data[:, 4]), vmax=np.nanmax(met_data[:, 4]), cmap='viridis_r')
ax21.scatter(met_data[:, 6], tri_data, c=met_data[:, 4], edgecolor='k', s=150, marker='o', vmin=np.nanmin(met_data[:, 4]), vmax=np.nanmax(met_data[:, 4]), cmap='viridis_r')

ax12.scatter(met_data[:, 7], kap_data, c=met_data[:, 4], edgecolor='k', s=150, marker='o', vmin=np.nanmin(met_data[:, 4]), vmax=np.nanmax(met_data[:, 4]), cmap='viridis_r')
ax22.scatter(met_data[:, 7], tri_data, c=met_data[:, 4], edgecolor='k', s=150, marker='o', vmin=np.nanmin(met_data[:, 4]), vmax=np.nanmax(met_data[:, 4]), cmap='viridis_r')

# Plot QHS Geometry Data #
ax11.scatter([-1], [1], c='k', edgecolor='w', s=750, marker='*')
ax21.scatter([-1], [1], c='k', edgecolor='w', s=750, marker='*')

ax12.scatter([-1], [1], c='k', edgecolor='w', s=750, marker='*')
ax22.scatter([-1], [1], c='k', edgecolor='w', s=750, marker='*')

# Axis Colorbar #
cbar = grid[0].cax.colorbar(smap)
cax = grid.cbar_axes[0]
cax.set_ylabel(r'$\mathcal{Q} / \mathcal{Q}^*$')

# Axis Labels #
ax21.set_xlabel(r'$\mathcal{B}_{\kappa} / |\mathcal{B}_{\kappa}^*|$')
ax22.set_xlabel(r'$\mathcal{C}_{n} / |\mathcal{C}_{n}^*|$')

ax11.set_ylabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax21.set_ylabel(r'$\delta/\delta^*$')

# Axis Text #
shift = 0.05

x11_lft, x11_rgt = ax11.get_xlim()
y11_bot, y11_top = ax11.get_ylim()
x11 = x11_lft + .5 * shift * (x11_rgt - x11_lft)
y11 = y11_top -.75 * shift * (y11_top - y11_bot)
ax11.text(x11, y11, '(a)', verticalalignment='top', horizontalalignment='left')

x12_lft, x12_rgt = ax12.get_xlim()
y12_bot, y12_top = ax12.get_ylim()
x12 = x12_rgt - 3 * shift * (x12_rgt - x12_lft)
y12 = y12_top - .75 * shift * (y12_top - y12_bot)
ax12.text(x12, y12, '(b)', verticalalignment='top', horizontalalignment='left')

x21_lft, x21_rgt = ax21.get_xlim()
y21_bot, y21_top = ax21.get_ylim()
x21 = x21_lft + .5 * shift * (x21_rgt - x21_lft)
y21 = y21_top - .75 * shift * (y21_top - y21_bot)
ax21.text(x21, y21, '(c)', verticalalignment='top', horizontalalignment='left')

x22_lft, x22_rgt = ax22.get_xlim()
y22_bot, y22_top = ax22.get_ylim()
x22 = x22_rgt - 3 * shift * (x22_rgt - x22_lft)
y22 = y22_top - .75 * shift * (y22_top - y22_bot)
ax22.text(x22, y22, '(d)', verticalalignment='top', horizontalalignment='left')

# Axis Grids #
ax11.grid()
ax12.grid()
ax21.grid()
ax22.grid()

plt.tight_layout()
# plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'new_metric_geom_comp.png')
plt.savefig(save_path)
