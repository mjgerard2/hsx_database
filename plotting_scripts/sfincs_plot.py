import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid


# Import metric data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data_scaled.h5')
met_dict = {}
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Import SFINCS Data #
sfincs_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'sfincs.h5')
with hf.File(sfincs_path, 'r') as hf_:
    sfincs_data = np.empty((met_data.shape[0], 4))
    for met_idx, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        sfincs_data[met_idx] = hf_[conID][:, 1]
    sfincs_qhs = hf_['QHS'][:, 1]
    rN_vals = hf_['rN values'][()]

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 22}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 26

# Plotting Axis #
fig = plt.figure(figsize=(10, 8))
grid = ImageGrid(fig, 111,
                 nrows_ncols=(2, 2),
                 axes_pad=0.5,
                 share_all=True,
                 aspect=False,
                 cbar_mode="single",
                 cbar_location="right",
                 cbar_size=0.35)

ax1 = grid[0]
ax2 = grid[1]
ax3 = grid[2]
ax4 = grid[3]

#fig, axs = plt.subplots(2, 2, sharex=True, sharey=False, tight_layout=False, figsize=(11, 8))

# Plot SFINCS Data #
smap = ax1.scatter(met_data[:, 9], sfincs_data[:, 0], c=met_data[:, 4], marker='o', s=150, edgecolor='k', cmap='viridis_r')
ax2.scatter(met_data[:, 9], sfincs_data[:, 1], c=met_data[:, 4], marker='o', s=150, edgecolor='k', cmap='viridis_r')
ax3.scatter(met_data[:, 9], sfincs_data[:, 2], c=met_data[:, 4], marker='o', s=150, edgecolor='k', cmap='viridis_r')
ax4.scatter(met_data[:, 9], sfincs_data[:, 3], c=met_data[:, 4], marker='o', s=150, edgecolor='k', cmap='viridis_r')

ax1.scatter([1.], [sfincs_qhs[0]], c='k', edgecolor='w', marker='*', s=750)
ax2.scatter([1.], [sfincs_qhs[1]], c='k', edgecolor='w', marker='*', s=750)
ax3.scatter([1.], [sfincs_qhs[2]], c='k', edgecolor='w', marker='*', s=750)
ax4.scatter([1.], [sfincs_qhs[3]], c='k', edgecolor='w', marker='*', s=750)

# Axis Limits #
max_flux = np.nanmax(sfincs_data)
ax1.set_ylim(0, 1.1*max_flux)
# ax1.set_xlim(0.955, 1.08)

# Colorbar #
cbar = grid[0].cax.colorbar(smap)
cax = grid.cbar_axes[0]
cax.set_ylabel(r'$\mathcal{Q} / \mathcal{Q}^*$')

# Axis Labels #
ax1.set_ylabel(r'$\langle Q_{\mathrm{e}} \rangle \ (\mathrm{kW}/\mathrm{m}^2)$')
ax3.set_ylabel(r'$\langle Q_{\mathrm{e}} \rangle \ (\mathrm{kW}/\mathrm{m}^2)$')

ax3.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax4.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

# Axis Titles #
ax1.set_title(r'$r/a = 0.2$')
ax2.set_title(r'$r/a = 0.4$')
ax3.set_title(r'$r/a = 0.6$')
ax4.set_title(r'$r/a = 0.8$')

# Axis Text #
shift = 0.05

x_lft, x_rgt = ax1.get_xlim()
y_bot, y_top = ax1.get_ylim()
x = x_lft + 3.5 * shift * (x_rgt - x_lft)
y = y_top - shift * (y_top - y_bot)

ax1.text(x, y, '(a)', verticalalignment='top', horizontalalignment='right')
ax2.text(x, y, '(b)', verticalalignment='top', horizontalalignment='right')
ax3.text(x, y, '(c)', verticalalignment='top', horizontalalignment='right')
ax4.text(x, y, '(d)', verticalalignment='top', horizontalalignment='right')

# Adjust Figure Layout #
plt.gcf().subplots_adjust(right=0.89)

# Axis Grid #
ax1.grid()
ax2.grid()
ax3.grid()
ax4.grid()

# Save or Show #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'sfincs.png')
plt.savefig(save_path)
# plt.show()
