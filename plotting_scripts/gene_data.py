import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid


# Import Metric Data #
metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'new_metric_data_scaled.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Metric Normalization #
metric_norm = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_normalizations.h5')
with hf.File(metric_norm, 'r') as hf_:
    qhs_tems = hf_['TEM'][()]

# Read in Growth Rates #
hf_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_1_x.h5')
# hf_path = os.path.join('/home', 'michael', 'Desktop', 'omega_data_5_x.h5')
with hf.File(hf_path, 'r') as hf_file:
    tem_data = np.full((100, 7, 3), np.nan)
    for m, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        tem_data[m] = hf_file[conID][()]

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 20}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 24

# Plotting Axis #
fig, axs = plt.subplots(1, 2, tight_layout=True, figsize=(14, 6))

ax1 = axs[0]
ax2 = axs[1]

# Plot Growth Rates #
ax1.scatter(met_data[:, 9], tem_data[:, 0, 1], marker='v', s=150, facecolor='None', edgecolor='tab:blue', label='0.1')
ax1.scatter(met_data[:, 9], tem_data[:, 1, 1], marker='o', s=150, facecolor='None', edgecolor='tab:orange', label='0.4')
ax1.scatter(met_data[:, 9], tem_data[:, 2, 1], marker='s', s=150, facecolor='None', edgecolor='tab:green', label='0.7')
ax1.scatter(met_data[:, 9], tem_data[:, 3, 1], marker='^', s=150, facecolor='None', edgecolor='tab:red', label='1.0')

ax1.scatter([1.], qhs_tems[0, 1], marker='*', s=750, c='tab:blue', edgecolor='k')
ax1.scatter([1.], qhs_tems[1, 1], marker='*', s=750, c='tab:orange', edgecolor='k')
ax1.scatter([1.], qhs_tems[2, 1], marker='*', s=750, c='tab:green', edgecolor='k')
ax1.scatter([1.], qhs_tems[3, 1], marker='*', s=750, c='tab:red', edgecolor='k')

# Plot Real Frequency #
ax2.scatter(met_data[:, 9], tem_data[:, 0, 2], marker='v', s=150, facecolor='None', edgecolor='tab:blue', label='0.1')
ax2.scatter(met_data[:, 9], tem_data[:, 1, 2], marker='o', s=150, facecolor='None', edgecolor='tab:orange', label='0.4')
ax2.scatter(met_data[:, 9], tem_data[:, 2, 2], marker='s', s=150, facecolor='None', edgecolor='tab:green', label='0.7')
ax2.scatter(met_data[:, 9], tem_data[:, 3, 2], marker='^', s=150, facecolor='None', edgecolor='tab:red', label='1.0')

ax2.scatter([1.], qhs_tems[0, 2], marker='*', s=750, c='tab:blue', edgecolor='k')
ax2.scatter([1.], qhs_tems[1, 2], marker='*', s=750, c='tab:orange', edgecolor='k')
ax2.scatter([1.], qhs_tems[2, 2], marker='*', s=750, c='tab:green', edgecolor='k')
ax2.scatter([1.], qhs_tems[3, 2], marker='*', s=750, c='tab:red', edgecolor='k')

# Axis Limits #
ax1.set_xlim(0.955, 1.08)
ax1.set_ylim(0, 0.38)

ax2.set_xlim(0.955, 1.08)
ax2.set_ylim(-0.9, 0.35)

# Axis Labels #
ax1.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax2.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax1.set_ylabel(r'$\gamma \ (c_s/a)$')
ax2.set_ylabel(r'$\omega \ (c_s/a)$')

# Axis Text #
x_txt = 0.957
ax1.text(x_txt, 0.01, '(a)')
ax2.text(x_txt, -0.87, '(b)')

# Axis Grids #
ax1.grid()
ax2.grid()

# Axis Legends #
ax2.legend(loc='upper center', frameon=False, ncol=4, columnspacing=0.5, handletextpad=0., title=r'$k_y$', fontsize=20)

# Plot/Save Figures #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'gene_data.png')
plt.savefig(save_path)
# plt.show()
