import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt


# Import Metric Data #
metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Metric Normalization #
metric_norm = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_normalizations.h5')
with hf.File(metric_norm, 'r') as hf_:
    qhs_tems = hf_['TEM'][()]

# Read in Growth Rates #
hf_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_1_x.h5')
with hf.File(hf_path, 'r') as hf_file:
    tem_data = np.full((100, 7, 3), np.nan)
    for m, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        tem_data[m] = hf_file[conID][()]

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22

# Plotting Axis #
"""
fig, axs = plt.subplots(2, 4, sharex=True, sharey=False, tight_layout=True, figsize=(20, 10))

ax11 = axs[0, 0]
ax12 = axs[0, 1]
ax13 = axs[0, 2]
ax14 = axs[0, 3]

ax21 = axs[1, 0]
ax22 = axs[1, 1]
ax23 = axs[1, 2]
ax24 = axs[1, 3]
"""
fig, axs = plt.subplots(2, 1, sharex=True, sharey=False, tight_layout=True, figsize=(8, 12))

ax11 = axs[0]
ax12 = ax11
ax13 = ax11
ax14 = ax11

ax21 = axs[1]
ax22 = ax21
ax23 = ax21
ax24 = ax21

# Plot Growth Rates #
ax11.scatter(met_data[:, 9], tem_data[:, 0, 1], marker='v', s=150, facecolor='None', edgecolor='tab:blue')  # , label=r'$k_y \rho_s = 0.1$')
ax12.scatter(met_data[:, 9], tem_data[:, 1, 1], marker='o', s=150, facecolor='None', edgecolor='tab:orange')  # , label=r'$k_y \rho_s = 0.4$')
ax13.scatter(met_data[:, 9], tem_data[:, 2, 1], marker='s', s=150, facecolor='None', edgecolor='tab:green')  # , label=r'$k_y \rho_s = 0.7$')
ax14.scatter(met_data[:, 9], tem_data[:, 3, 1], marker='^', s=150, facecolor='None', edgecolor='tab:red')  # , label=r'$k_y \rho_s = 1.0$')

ax11.scatter([1.], qhs_tems[0, 1], marker='*', s=500, c='k', edgecolor='w')
ax12.scatter([1.], qhs_tems[1, 1], marker='*', s=500, c='k', edgecolor='w')
ax13.scatter([1.], qhs_tems[2, 1], marker='*', s=500, c='k', edgecolor='w')
ax14.scatter([1.], qhs_tems[3, 1], marker='*', s=500, c='k', edgecolor='w')

# Plot Omegas #
ax21.scatter(met_data[:, 9], tem_data[:, 0, 2], marker='v', s=150, facecolor='None', edgecolor='tab:blue')
ax22.scatter(met_data[:, 9], tem_data[:, 1, 2], marker='o', s=150, facecolor='None', edgecolor='tab:orange')
ax23.scatter(met_data[:, 9], tem_data[:, 2, 2], marker='s', s=150, facecolor='None', edgecolor='tab:green')
ax24.scatter(met_data[:, 9], tem_data[:, 3, 2], marker='^', s=150, facecolor='None', edgecolor='tab:red')

ax21.scatter([1.], qhs_tems[0, 2], marker='*', s=500, c='k', edgecolor='w')
ax22.scatter([1.], qhs_tems[1, 2], marker='*', s=500, c='k', edgecolor='w')
ax23.scatter([1.], qhs_tems[2, 2], marker='*', s=500, c='k', edgecolor='w')
ax24.scatter([1.], qhs_tems[3, 2], marker='*', s=500, c='k', edgecolor='w')

# Axis Limits #
ax11.set_xlim(0.955, 1.08)

ax11.set_ylim(0, 0.38)
ax21.set_ylim(-0.84, 0)
"""
ax11.set_ylim(0, 1.1*np.nanmax(tem_data[:, 0, 1]))
ax12.set_ylim(0, 1.1*np.nanmax(tem_data[:, 1, 1]))
ax13.set_ylim(0, 1.1*np.nanmax(tem_data[:, 2, 1]))
ax14.set_ylim(0, 1.1*np.nanmax(tem_data[:, 3, 1]))

ax21.set_ylim(1.1*np.nanmin(tem_data[:, 0, 2]), 0)
ax22.set_ylim(1.1*np.nanmin(tem_data[:, 1, 2]), 0)
ax23.set_ylim(1.1*np.nanmin(tem_data[:, 2, 2]), 0)
ax24.set_ylim(1.1*np.nanmin(tem_data[:, 3, 2]), 0)
"""
# Axis Titles #
"""
ax11.set_title(r'$k_y \rho_s = 0.1$')
ax12.set_title(r'$k_y \rho_s = 0.4$')
ax13.set_title(r'$k_y \rho_s = 0.7$')
ax14.set_title(r'$k_y \rho_s = 1.0$')
"""
# Axis Labels #
ax11.set_ylabel(r'$\gamma \ (c_s/a)$')
ax21.set_ylabel(r'$\omega \ (c_s/a)$')

ax21.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax22.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax23.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax24.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

# Axis Grids #
ax11.grid()
# ax12.grid()
# ax13.grid()
# ax14.grid()

ax21.grid()
# ax22.grid()
# ax23.grid()
# ax24.grid()
"""
# Axis Legends #
ax11.legend()
ax12.legend()
ax13.legend(loc='lower left')
ax14.legend()
"""
# Plot/Save Figures #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'linear_gene.png')
plt.savefig(save_path)
# plt.show()
