import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import ImageGrid


# Import Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

met_data = met_data[met_data[:, 3] <= 10]
met_data = np.array(sorted(met_data, key=lambda x: x[3], reverse=True))

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 32}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 36

# Plotting Axis #
fig = plt.figure(1, figsize=(10, 12))
grid = ImageGrid(fig, 111,
                 nrows_ncols=(2, 1),
                 axes_pad=0.25,
                 aspect=False,
                 cbar_mode="single",
                 cbar_location="right",
                 cbar_size=0.35)

ax1 = grid[0]
ax2 = grid[1]

D_kappa = np.max(met_data[:, 9]) - np.min(met_data[:, 9])
D_Fk = np.max(met_data[:, 6]) - np.min(met_data[:, 6])
D_Gp = np.max(met_data[:, 7]) - np.min(met_data[:, 7])

asp = 0.85
ax1.set_aspect(asp*(D_kappa/D_Gp))
ax2.set_aspect(asp*(D_kappa/D_Fk))

# Plot Metrics #
cmap1 = ax1.scatter(met_data[:, 9], met_data[:, 7], c=met_data[:, 3], s=1, norm=LogNorm(vmin=np.min(met_data[:, 3]), vmax=10), cmap='viridis_r')
# cmap1 = ax1.scatter(met_data[:, 9], met_data[:, 7], c=met_data[:, 3], s=1, vmin=np.min(met_data[:, 3]), vmax=10, cmap='viridis_r')
ax1.scatter([1.], [1.], c='k', edgecolor='w', marker='*', s=1000)

cmap2 = ax2.scatter(met_data[:, 9], met_data[:, 6], c=met_data[:, 3], s=1, norm=LogNorm(vmin=np.min(met_data[:, 3]), vmax=10), cmap='viridis_r')
# cmap2 = ax2.scatter(met_data[:, 9], met_data[:, 6], c=met_data[:, 3], s=1, vmin=np.min(met_data[:, 3]), vmax=10, cmap='viridis_r')
ax2.scatter([1.], [1.], c='k', edgecolor='w', marker='*', s=1000)

# Axis Colorbar #
cbar = grid[0].cax.colorbar(cmap1)
cax = grid.cbar_axes[0]
cax.set_ylabel(r'$\mathcal{E} / \mathcal{E}^*$')

# Axis Labels #
ax1.set_ylabel(r'$\mathcal{G}_{\psi} / \mathcal{G}_{\psi}^*$')
ax2.set_ylabel(r'$\mathcal{F}_{\kappa} / \mathcal{F}_{\kappa}^*$')
ax2.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

# Axis Text #
x_txt = 1.064
ax1.text(x_txt, 0.37, '(a)')
ax2.text(x_txt, 0.913, '(b)')

# Axis Limits #
ax1.set_xlim(0.95, 1.08)
ax2.set_xlim(0.95, 1.08)

# Axis Grids #
ax1.grid()
ax2.grid()

# Save/Show #
plt.tight_layout()
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'metric_kappa.png')
plt.savefig(save_path)
# plt.show()
