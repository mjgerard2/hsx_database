import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.gridspec as gsp
import matplotlib.pyplot as plt

from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import ImageGrid

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

from vmecTools.wout_files import wout_read as wr
from vmecTools.wout_files import curveB_tools as curB


def get_extrema(data):
    data_min = np.inf
    data_max = -np.inf
    for key in data:
        if np.min(data[key]) < data_min:
            data_min = np.min(data[key])
        if np.max(data[key]) > data_max:
            data_max = np.max(data[key])
    return data_min, data_max


tor_angle = 0.0*np.pi

# Import Metric Data #
metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Identify Configurations to Plot #
idx_min = np.argmin(met_data[:, 9])
idx_max = np.argmax(met_data[:, 9])

kappa_min = met_data[idx_min, 9]
kappa_max = met_data[idx_max, 9]

conID_min = '-'.join(['{0:0.0f}'.format(iD) for iD in met_data[idx_min, 0:3]])
conID_max = '-'.join(['{0:0.0f}'.format(iD) for iD in met_data[idx_max, 0:3]])

conIDs = [conID_min, '0-1-0', conID_max]
kappa_values = np.array([kappa_min, 1., kappa_max])

# Import Vessel Data #
path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'vessel90.h5')
with hf.File(path,'r') as hf_file:
    vessel = hf_file['data'][:]
    v_dom = hf_file['domain'][:]

idx = np.argmin(np.abs(v_dom - tor_angle))
ves_x = vessel[idx, :, 0]
ves_y = vessel[idx, :, 1]
ves_z = vessel[idx, :, 2]
ves_r = np.hypot(ves_x, ves_y)

# Get Surface Data #
psi_dom = np.linspace(.2, 1, 5)**2
pol_dom = np.linspace(-np.pi, np.pi, 501)
flux_surf = np.empty((len(conIDs), psi_dom.shape[0], pol_dom.shape[0], 2))

k_n = {}
g_ss = {}
B_mod = {}

R_grid = {}
Z_grid = {}

R_ft = {}
Z_ft = {}

for cdx, conID in enumerate(conIDs):
    print(conID+' ({0:0.0f}|{1:0.0f})'.format(cdx+1, len(conIDs)))
    conArr = np.array([float(iD) for iD in conID.split('-')])
    mainID = 'main_coil_{0:0.0f}'.format(conArr[0])
    setID = 'set_{0:0.0f}'.format(conArr[1])
    jobID = 'job_{0:0.0f}'.format(conArr[2])
    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID)

    wout = wr.readWout(wout_path, space_derivs=True, field_derivs=True)
    beg_idx = np.argmin(np.abs(wout.s_grid - 0.2**2))

    # Get <B> #
    u_dom = np.linspace(-np.pi, np.pi, 101)
    v_dom = np.linspace(-np.pi, np.pi, u_dom.shape[0]*4)
    wout.transForm_3D(wout.s_grid[beg_idx::], u_dom, v_dom, ['Bmod', 'Jacobian'])

    Bmod = wout.invFourAmps['Bmod']
    jacob = wout.invFourAmps['Jacobian']

    numer = np.trapz(np.trapz(Bmod*jacob, u_dom, axis=2), v_dom, axis=1)
    denom = np.trapz(np.trapz(jacob, u_dom, axis=2), v_dom, axis=1)

    B_avg = np.empty((wout.s_grid[beg_idx::].shape[0], pol_dom.shape[0]))
    for s_idx in range(wout.s_grid[beg_idx::].shape[0]):
        B_avg[s_idx] = numer[s_idx]/denom[s_idx]

    # Calculate \kappa_n, g_{\psi \psi}, and (B - <B>)/<B> #
    ampKeys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv',
               'Bmod', 'Bs_covar', 'Bu_covar', 'Bv_covar',
               'dBmod_ds', 'dBmod_du', 'dBmod_dv',
               'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']

    wout.transForm_2D_vSec(wout.s_grid[beg_idx::], pol_dom, tor_angle, ampKeys)
    curve = curB.BcurveTools(wout)
    curve.calc_curvature()

    k_n[conID] = curve.k_s
    g_ss[conID] = (wout.invFourAmps['dR_ds']**2 + wout.invFourAmps['dZ_ds']**2) / (wout.phi_dom[-1]**2)
    B_mod[conID] = (wout.invFourAmps['Bmod'] - B_avg) / B_avg

    for psi_idx, psi_val in enumerate(psi_dom):
        idx = np.argmin(np.abs(wout.s_grid[beg_idx::] - psi_val))
        flux_surf[cdx, psi_idx] = np.stack((wout.invFourAmps['R'][idx], wout.invFourAmps['Z'][idx]), axis=1)

    R_grid[conID] = wout.invFourAmps['R']
    Z_grid[conID] = wout.invFourAmps['Z']

    # Find Flux Tube Center #
    wout.transForm_1D(0.5, 0, tor_angle, ['R', 'Z'])
    R_ft[conID] = wout.invFourAmps['R']
    Z_ft[conID] = wout.invFourAmps['Z']

k_n_min, k_n_max = get_extrema(k_n)
k_n_lim = np.max(np.abs(np.array([k_n_min, k_n_max])))

B_mod_min, B_mod_max = get_extrema(B_mod)
B_mod_lim = np.max(np.abs(np.array([B_mod_min, B_mod_max])))

g_ss_min, g_ss_max = get_extrema(g_ss)

# Plot Axis #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 14}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['lines.linewidth'] = 1

# Plotting Axis #
fig = plt.figure(figsize=(10, 12))
axs = ImageGrid(fig, 111,
                nrows_ncols=(3, 3),
                axes_pad=0.25,
                aspect=True,
                cbar_location='right',
                cbar_mode='edge',
                cbar_size='10%',
                cbar_pad=0.15)

# Plot k_n surfaces #
axs[0].pcolormesh(R_grid[conIDs[0]], Z_grid[conIDs[0]], k_n[conIDs[0]], vmin=-k_n_lim, vmax=k_n_lim, cmap='seismic')
axs[1].pcolormesh(R_grid[conIDs[1]], Z_grid[conIDs[1]], k_n[conIDs[1]], vmin=-k_n_lim, vmax=k_n_lim, cmap='seismic')
im_kn = axs[2].pcolormesh(R_grid[conIDs[2]], Z_grid[conIDs[2]], k_n[conIDs[2]], vmin=-k_n_lim, vmax=k_n_lim, cmap='seismic')

# Plot (B - <B>)/<B> surfaces #
axs[3].pcolormesh(R_grid[conIDs[0]], Z_grid[conIDs[0]], B_mod[conIDs[0]], vmin=-B_mod_lim, vmax=B_mod_lim, cmap='RdBu_r')
axs[4].pcolormesh(R_grid[conIDs[1]], Z_grid[conIDs[1]], B_mod[conIDs[1]], vmin=-B_mod_lim, vmax=B_mod_lim, cmap='RdBu_r')
im_B = axs[5].pcolormesh(R_grid[conIDs[2]], Z_grid[conIDs[2]], B_mod[conIDs[2]], vmin=-B_mod_lim, vmax=B_mod_lim, cmap='RdBu_r')

# Plot g_ss surfaces #
axs[6].pcolormesh(R_grid[conIDs[0]], Z_grid[conIDs[0]], g_ss[conIDs[0]], norm=LogNorm(vmin=g_ss_min, vmax=g_ss_max), cmap='magma')
axs[7].pcolormesh(R_grid[conIDs[1]], Z_grid[conIDs[1]], g_ss[conIDs[1]], norm=LogNorm(vmin=g_ss_min, vmax=g_ss_max), cmap='magma')
im_gss = axs[8].pcolormesh(R_grid[conIDs[2]], Z_grid[conIDs[2]], g_ss[conIDs[2]], norm=LogNorm(vmin=g_ss_min, vmax=g_ss_max), cmap='magma')

for i in range(3):
    for cdx, conID in enumerate(conIDs):
        idx = i*len(conIDs) + cdx
        axs[idx].plot(ves_r, ves_z, c='k', linewidth=3)
        for pdx in range(psi_dom.shape[0]):
            if pdx == psi_dom.shape[0]-1 or pdx == 0:
                axs[idx].plot(flux_surf[cdx, pdx, :, 0], flux_surf[cdx, pdx, :, 1], c='w', linewidth=2)
                axs[idx].scatter(R_ft[conID], Z_ft[conID], s=100, marker='X', c='k', zorder=25)
            else:
                axs[idx].plot(flux_surf[cdx, pdx, :, 0], flux_surf[cdx, pdx, :, 1], c='w')

        # Axis Titles #
        if i == 0:
            axs[idx].set_title(r'$\mathcal{{K}} / \mathcal{{K}}^* = {0:0.2f}$'.format(kappa_values[cdx]))

# Axis Colorbar #
plt.colorbar(im_kn, cax=axs.cbar_axes[0])
axs.cbar_axes[0].set_ylabel(r'$\kappa_{n}$')

plt.colorbar(im_B, cax=axs.cbar_axes[1])
# axs.cbar_axes[1].set_ylabel(r'$\left(B - \langle B \rangle \right) / \langle B \rangle$')
axs.cbar_axes[1].set_ylabel(r'$\left(B - \langle B \rangle\right) / \langle B \rangle}$')

plt.colorbar(im_gss, cax=axs.cbar_axes[2])
axs.cbar_axes[2].set_ylabel(r'$g_{\psi\psi}$')

# Axis Labels #
axs[0].set_ylabel('Z')
axs[3].set_ylabel('Z')
axs[6].set_ylabel('Z')

axs[6].set_xlabel('R')
axs[7].set_xlabel('R')
axs[8].set_xlabel('R')

# Axis Limits #
"""
axs[0].set_xlim(1.25, 1.63)
axs[1].set_xlim(1.25, 1.63)
axs[2].set_xlim(1.25, 1.63)

axs[3].set_xlim(1.25, 1.63)
axs[4].set_xlim(1.25, 1.63)
axs[5].set_xlim(1.25, 1.63)

axs[6].set_xlim(1.25, 1.63)
axs[7].set_xlim(1.25, 1.63)
axs[8].set_xlim(1.25, 1.63)

axs[0].set_ylim(-.3, .3)
axs[1].set_ylim(-.3, .3)
axs[2].set_ylim(-.3, .3)

axs[3].set_ylim(-.3, .3)
axs[4].set_ylim(-.3, .3)
axs[5].set_ylim(-.3, .3)

axs[6].set_ylim(-.3, .3)
axs[7].set_ylim(-.3, .3)
axs[8].set_ylim(-.3, .3)

# Axis Text #
x_txt, y_txt = 1.57, -0.28
axs[0].text(x_txt, y_txt, '(a)')
axs[1].text(x_txt, y_txt, '(b)')
axs[2].text(x_txt, y_txt, '(c)')

axs[3].text(x_txt, y_txt, '(d)')
axs[4].text(x_txt, y_txt, '(e)')
axs[5].text(x_txt, y_txt, '(f)')

axs[6].text(x_txt, y_txt, '(g)')
axs[7].text(x_txt, y_txt, '(h)')
axs[8].text(x_txt, y_txt, '(i)')
"""
# Save/Show Figure #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'elongation_sample.png')
# plt.savefig(save_path)
plt.show()
