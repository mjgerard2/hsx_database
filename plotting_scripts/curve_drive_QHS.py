import os
import numpy as np
import matplotlib.pyplot as plt

import sys
modDIRC = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(modDIRC)

import plot_define as pd
import gistTools.gist_reader as gr


# Gist path #
gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'gist_QHS_best_alf0_s0p5_res1024_npol4.dat')
gist = gr.read_gist(gist_path)

pol_lim = 2*np.pi
idx_above = np.where(gist.pol_dom >= -pol_lim)[0]
idx_below = np.where(gist.pol_dom <= pol_lim)[0]
idx = np.intersect1d(idx_above, idx_below)

pol_norm = gist.pol_dom[idx] / np.pi

B = gist.data['modB'][idx]
sqrt_gxx = np.sqrt(gist.data['gxx'][idx])
Kn = gist.data['Kn'][idx]
Kg = gist.data['Kg'][idx]
D = gist.data['D'][idx]

Kn_term = Kn/sqrt_gxx
curve_drive = (Kn/sqrt_gxx) + (D*Kg*(sqrt_gxx/B))

# Plot Results #
plot = pd.plot_define(fontSize=24, labelSize=28, lineWidth=3)
fig, ax1 = plt.subplots(1, 1, tight_layout=True, figsize=(18, 4))
ax2 = ax1.twinx()

ax1.plot(pol_norm, B, c='tab:blue')
ax2.plot(pol_norm, curve_drive, c='tab:orange', label=r'$C_d$')
# ax2.plot(pol_norm, Kn_term, c='tab:orange', ls='--', label=r'$C_d - D\kappa_g \frac{|\nabla \psi|}{B}$')
ax2.plot(pol_norm, Kn_term, c='tab:orange', ls='--', label=r'$\kappa_n / |\nabla \psi|$')
# ax2.plot(pol_norm, curve_drive, c='tab:orange', label=r'$\frac{\kappa_n}{|\nabla \psi|} + D\kappa_g \frac{|\nabla \psi|}{B}$')
# ax2.plot(pol_norm, Kn_term, c='tab:orange', ls='--', label=r'$\frac{\kappa_n}{|\nabla \psi|}$')

ax1.spines['left'].set_color('tab:blue')
ax1.tick_params(axis='y', colors='tab:blue')

ax2.spines['right'].set_color('tab:orange')
ax2.tick_params(axis='y', colors='tab:orange')

ax1.set_xlabel(r'$\lambda / \pi$')
ax1.set_ylabel(r'$B \ (T)$', color='tab:blue')

B_avg = np.mean(B)
B_max = 1.5 * np.max(np.abs(B-B_avg))
Cd_max = 1.15*np.max(np.abs(np.r_[curve_drive, Kn_term]))
ax1.set_xlim(-pol_lim/np.pi, pol_lim/np.pi)
# ax1.set_ylim(B_avg-B_max, B_avg+B_max)
ax2.set_ylim(-Cd_max, Cd_max)

ax2.legend(loc='lower right', ncol=2)
ax2.grid()

# plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'curvature_drive.png')
# save_path = os.path.join('/home', 'michael', 'Desktop', 'Prelim_Defense', 'figures', 'fieldline_modB_curveDrive.png')
plt.savefig(save_path)
