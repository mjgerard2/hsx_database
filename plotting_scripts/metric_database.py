import os, sys
import numpy as np
import h5py as hf

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

PATH = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'analysis_scripts')
sys.path.append(PATH)

import box_class as bc


# Read in Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

is_nan = np.isnan(met_data[:, 3])
not_nan = ~is_nan
met_data[not_nan, :]

is_nan = np.isnan(met_data[:, 4])
not_nan = ~is_nan
met_data[not_nan, :]

met_data = np.array(sorted(met_data, key=lambda x: x[3], reverse=True))

# Generate Plot #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 24}

mpl.rc('font', **font)
mpl.rcParams['axes.labelsize'] = 28
mpl.rcParams['lines.linewidth'] = 1

# Plotting Axis #
fig, axs = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))

smap = axs.scatter(met_data[:, 6], met_data[:, 7], c=met_data[:, 3], s=1, norm=LogNorm(vmin=np.min(met_data[:, 3]), vmax=np.max(met_data[:, 3])), cmap='viridis_r')

box = bc.read_boxes()
box.plot_boxes(axs)

axs.scatter([1.], [1.], c='k', edgecolor='w', marker='*', s=500, zorder=10)

# Axis Labels #
cbar = fig.colorbar(smap, ax=axs)
cbar.ax.set_ylabel(r'$\mathcal{E} / \mathcal{E}^*$')

# Save/Show #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'metric_database.png')
# plt.savefig(save_path)
plt.show()
