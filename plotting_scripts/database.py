import os
import numpy as np
import h5py as hf

import matplotlib as mpl
import matplotlib.gridspec as gs
import matplotlib.pyplot as plt

from matplotlib.patches import ConnectionPatch
from matplotlib.colors import LogNorm


# Read in Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

is_nan = np.isnan(met_data[:, 3])
not_nan = ~is_nan
met_data[not_nan, :]

is_nan = np.isnan(met_data[:, 4])
not_nan = ~is_nan
met_data[not_nan, :]

met_data = np.array(sorted(met_data, key=lambda x: x[3], reverse=True))

# Read in GENE Configurations #
con_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'conID_list.txt')
with open(con_path, 'r') as f:
    lines = f.readlines()
    conARR = np.empty((len(lines), 3))
    for l, line in enumerate(lines):
        conARR[l] = np.array([float(ID) for ID in line.strip().split("-")])

gene_data = np.empty((conARR.shape[0], met_data.shape[1]))
for i, conArr in enumerate(conARR):
    idx = np.argmin(np.sum(np.abs(met_data[:, 0:3] - conArr), axis=1))
    gene_data[i] = met_data[idx]

# Generate Plot #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)
mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['lines.linewidth'] = 2

fig = plt.figure(constrained_layout=True, figsize=(8, 8))
spec = gs.GridSpec(3, 1, figure=fig)

ax1 = fig.add_subplot(spec[0:2])
ax2 = fig.add_subplot(spec[2])

Q_low, Q_hgh = 0.5, 4.5
K_low, K_hgh = 0.95, 1.08

# Full Database #
smap1 = ax1.scatter(met_data[:, 9], met_data[:, 4], c=met_data[:, 3], s=1, norm=LogNorm(), cmap='viridis_r')
ax1.scatter([1.], [1.], c='k', s=200, marker='*', edgecolor='w')

# Full Database Box #
ax1.plot([K_low, K_hgh], [Q_low, Q_low], c='k', ls='--')
ax1.plot([K_low, K_hgh], [Q_hgh, Q_hgh], c='k', ls='--')
ax1.plot([K_low, K_low], [Q_low, Q_hgh], c='k', ls='--')
ax1.plot([K_hgh, K_hgh], [Q_low, Q_hgh], c='k', ls='--')
cmap1 = fig.colorbar(smap1, ax=ax1)
cmap1.ax.set_ylabel(r'$\mathcal{E} / \mathcal{E}^*$')

# Subset Database #
sub_met_data = met_data[met_data[:, 4] < Q_hgh, :]
ax2.scatter(sub_met_data[:, 9], sub_met_data[:, 4], c=sub_met_data[:, 3], s=1, norm=LogNorm(vmin=np.min(sub_met_data[:, 3]), vmax=np.max(sub_met_data[:, 3])), cmap='binary')
ax2.scatter(gene_data[:, 9], gene_data[:, 4], c=gene_data[:, 3], edgecolor='k', s=150, marker='s', norm=LogNorm(vmin=np.min(met_data[:, 3]), vmax=np.max(met_data[:, 3])), cmap='viridis_r')
ax2.scatter([1.], [1.], c='k', s=500, marker='*', edgecolor='w')

# Draw Connections #
con1 = ConnectionPatch(xyA=(K_low, Q_low), xyB=(1.01*K_low, 0.99*Q_hgh), coordsA='data', coordsB='data', axesA=ax1, axesB=ax2, color='k')
ax1.add_artist(con1)

con2 = ConnectionPatch(xyA=(K_hgh, Q_low), xyB=(.99*K_hgh, .99*Q_hgh), coordsA='data', coordsB='data', axesA=ax1, axesB=ax2, color='k')
ax1.add_artist(con2)

# Write figure text labels #
ax1.text(.88, .9, '(a)')
ax2.text(.952, .85, '(b)')

ax1.set_ylim(0, np.nanmax(met_data[:, 4]))
ax2.set_ylim(Q_low, Q_hgh)
ax2.set_xlim(K_low, K_hgh)

ax1.set_ylabel(r'$\mathcal{Q} / \mathcal{Q}^*$')
ax2.set_ylabel(r'$\mathcal{Q} / \mathcal{Q}^*$')
ax2.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

#plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'database.png')
# plt.savefig(save_path)
plt.show()
