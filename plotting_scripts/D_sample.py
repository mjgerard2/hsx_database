import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

from databaseTools.functions import readCrntConfig
from vmecTools.wout_files import wout_read as wr
from flfTools import flf_class as flf


rots = 500
tor_value = 0.24*np.pi
roa_values = np.array([0.2, 0.4, 0.6, 0.8])
chi_values = np.array([1, 10, 100])

# Read in Chi-Squared Data #
hf_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'D_test.h5')
with hf.File(hf_path, 'r') as hf_file:
    # chi_data = np.empty((100, 7))
    chi_dict = {}
    chi_data = np.empty((100, 4))
    for m, key in enumerate(hf_file):
        conArr = np.array([float(iD) for iD in key.split('-')])
        chi_dict[key] = hf_file[key][()]
        chi_norm = np.linalg.norm(hf_file[key][:], axis=1)
        chi_data[m] = np.r_[conArr, np.sqrt(np.sum(chi_norm*chi_norm))]

# Find surface indices with chi-squared values #
# nearest to the elements of chi_values #
chi_keys = []
chi_indices = np.empty(chi_values.shape[0], dtype=int)
for chi_idx, chi_val in enumerate(chi_values):
    idx = np.nanargmin(np.abs(chi_data[:, 3] - chi_val))
    chi_indices[chi_idx] = idx
    conID = '-'.join(['{0:0.0f}'.format(iD) for iD in chi_data[idx, 0:3]])
    chi_keys.append(conID)

# Import VMEC Data and Calculate FLF Data #
pol_values = np.linspace(-np.pi, np.pi, 151)
vmec_surf = np.empty((chi_values.shape[0], roa_values.shape[0], 151, 2))
flf_surf = np.empty((chi_values.shape[0], roa_values.shape[0], rots-1, 2))
for chi_idx, chi_val in enumerate(chi_values):
    print('({0:0.0f}|{1:0.0f})'.format(chi_idx+1, chi_values.shape[0]))

    conArr = chi_data[chi_indices[chi_idx], 0:3]
    mainID = 'main_coil_{0:0.0f}'.format(conArr[0])
    setID = 'set_{0:0.0f}'.format(conArr[1])
    jobID = 'job_{0:0.0f}'.format(conArr[2])
    con_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID)

    conID = '-'.join(['{0:0.0f}'.format(iD) for iD in conArr])
    main, aux = readCrntConfig(conID)
    crnt = -10722. * np.r_[main, 14*aux]
    crnt_str = ' '.join(['{}'.format(c) for c in crnt])

    dphi = 0.125*np.pi
    stps = int(2*np.pi/dphi)
    npts = int(rots*stps)

    mod_dict = {'points_dphi': dphi,
                'n_iter': npts,
                'mgrid_currents': crnt_str}

    flf_hsx = flf.flf_wrapper('HSX')
    flf_hsx.change_params(mod_dict)

    # Read Out FLF Parameters For Indexing #
    rots = int((flf_hsx.params['n_iter'] * flf_hsx.params['points_dphi']) / (2*np.pi))
    idx_stps = np.array(np.arange(rots)*stps, dtype=int)

    # VMEC Data #
    wout = wr.readWout(con_path)
    wout.transForm_2D_vSec(roa_values**2, pol_values, tor_value, ['R', 'Z'])
    vmec_surf[chi_idx] = np.stack((wout.invFourAmps['R'][:], wout.invFourAmps['Z'][:]), axis=2)

    for roa_idx, roa_val in enumerate(roa_values):
        init_point = np.array([vmec_surf[chi_idx, roa_idx, 0, 0], vmec_surf[chi_idx, roa_idx, 0, 1], tor_value])
        points = flf_hsx.execute_flf(init_point, DeBug=True)[idx_stps]
        flf_surf[chi_idx, roa_idx] = np.stack((points[:, 0], points[:, 1]), axis=1)

# Plot Axis #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Plotting Axis #
fig, axs = plt.subplots(3, 1, sharex=True, sharey=True, tight_layout=True, figsize=(9, 10))
axs[0].set_aspect('equal')
axs[1].set_aspect('equal')
axs[2].set_aspect('equal')

# Plot Surfaces #
ls = ['-', '--', '-.', ':']
for a, chi_val in enumerate(chi_values):
    for b, roa_val in enumerate(roa_values):
        D_val = np.linalg.norm(chi_dict[chi_keys[a]][b])
        axs[a].plot(vmec_surf[a, b, :, 0], vmec_surf[a, b, :, 1], c='tab:blue', ls=ls[b], label=r'$D = {0:0.2g} \ \%$'.format(D_val))
        axs[a].scatter(flf_surf[a, b, :, 0], flf_surf[a, b, :, 1], c='tab:orange', marker='o', s=1)

    # Axis Titles #
    # axs[a].set_title(r'$|D| = {0:0.0f} \ \%$'.format(chi_data[chi_indices[a], 3]))

# Axis Limits #
axs[0].set_xlim(0.99*np.min(flf_surf[:, :, :, 0]), 1.4)

# Axis Labels #
axs[0].set_ylabel('Z')
axs[1].set_ylabel('Z')
axs[2].set_ylabel('Z')
axs[2].set_xlabel('R')

# Axis Legends #
axs[0].legend()
axs[1].legend()
axs[2].legend()

# Axis Text #
x_txt, y_txt = 1.37, -0.073
axs[0].text(x_txt, y_txt, '(a)')
axs[1].text(x_txt, y_txt, '(b)')
axs[2].text(x_txt, y_txt, '(c)')

# Save/Show Figure #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'D_sample.png')
plt.savefig(save_path)
# plt.show()
