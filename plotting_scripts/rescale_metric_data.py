import os
import h5py as hf
import numpy as np
import matplotlib.pyplot as plt

import sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd
from vmecTools.wout_files import wout_read as wr


rng = int(1e3)

# Import Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Get QHS Volume #
wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0')
wout = wr.readWout(wout_path)
vol_qhs = wout.volume

# Create Scaled Data File #
met_path_scaled = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data_scaled.h5')
if os.path.isfile(met_path_scaled):
    with hf.File(met_path_scaled, 'r') as hf_:
        met_scaled = hf_['metric data'][()]

    is_zero = np.where(met_scaled[:, 3] == 0)[0]
    num_of_configs = met_scaled[is_zero].shape[0]
    ibase = met_data.shape[0] - num_of_configs

    met_1k_cnt = int(num_of_configs / rng)
    met_1k_remain = num_of_configs % rng

else:
    met_scaled = np.full(met_data.shape, np.nan)
    with hf.File(met_path_scaled, 'w') as hf_:
        hf_.create_dataset('metric data', data=np.zeros(met_data.shape))

    ibase = 0
    met_1k_cnt = int(met_data.shape[0] / rng)
    met_1k_remain = met_data.shape[0] % rng

# Rescale Metric Data #
for i in range(met_1k_cnt):
    print('({0:0.0f}|{1:0.0f})'.format(i+1, met_1k_cnt))
    for j in range(rng):
        idx = ibase + i*rng + j
        met = met_data[idx, :]

        mainID = 'main_coil_{0:0.0f}'.format(met[0])
        setID = 'set_{0:0.0f}'.format(met[1])
        jobID = 'job_{0:0.0f}'.format(met[2])

        wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID)
        wout = wr.readWout(wout_path)
        vol = wout.volume

        met_scaled[idx, 0:3] = met[0:3]
        met_scaled[idx, 3::] = met[3::] * (vol_qhs/vol)

    ibeg = ibase + int(i*rng)
    iend = ibase + int((i+1)*rng)

    with hf.File(met_path_scaled, 'a') as hf_:
        met_temp = hf_['metric data']
        met_temp[ibeg:iend] = met_scaled[ibeg:iend]

print('Remainder . . .')
ibase += int(met_1k_cnt*rng)
for i in range(met_1k_remain):
    idx = ibase + i
    met = met_data[idx, :]

    mainID = 'main_coil_{0:0.0f}'.format(met[0])
    setID = 'set_{0:0.0f}'.format(met[1])
    jobID = 'job_{0:0.0f}'.format(met[2])

    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID)
    wout = wr.readWout(wout_path)
    vol = wout.volume

    met_scaled[idx, 0:3] = met[0:3]
    met_scaled[idx, 3::] = met[3::] * (vol_qhs/vol)

ilast = met_data.shape[0]
with hf.File(met_path_scaled, 'a') as hf_:
    met_temp = hf_['metric data']
    met_temp[ibase:ilast] = met_scaled[ibase:ilast]

# Plot Data #
plot = pd.plot_define()
fig, ax = plt.subplots(1, 1, tight_layout=True)

ax.scatter(met_data[:, 7], met_scaled[:, 7], c='None', edgecolor='k', s=75)

xmin, xmax = ax.get_xlim()
ymin, ymax = ax.get_ylim()
ax.plot([xmin, xmax], [xmin, xmax], c='k', ls='--')

ax.set_xlabel('Old Met. Data')
ax.set_ylabel('Vol. Sc. Met. Data')

ax.grid()
# plt.show()
met_fig_scaled = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data_scaled.png')
plt.savefig(met_fig_scaled)
