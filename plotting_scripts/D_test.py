import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm


base_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley')

# Import Metric Data #
metric_path = os.path.join(base_path, 'data_files', 'metric_data.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Read in Difference Data #
hf_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'D_test.h5')
with hf.File(hf_path, 'r') as hf_file:
    chi_data = np.full((met_data.shape[0], 4), np.nan)
    for m, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        chi_data[m] = np.linalg.norm(hf_file[conID][:], axis=1)

# Read in Difference Data for QHS #
hf_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'D_test_QHS.h5')
with hf.File(hf_path, 'r') as hf_file:
    chi_qhs_data = np.linalg.norm(hf_file['0-1-0'][:], axis=1)

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22

# Plotting Axis #
fig, axs = plt.subplots(2, 2, sharex=True, sharey=True, tight_layout=False, figsize=(10, 8))

ax1 = axs[0, 0]
ax2 = axs[0, 1]
ax3 = axs[1, 0]
ax4 = axs[1, 1]

# Plot GENE Data #
ax1.scatter(met_data[:, 9], chi_data[:, 0], marker='v', s=150, facecolor='None', edgecolor='tab:blue', label=r'$r/a = 0.2$')
ax2.scatter(met_data[:, 9], chi_data[:, 1], marker='o', s=150, facecolor='None', edgecolor='tab:orange', label=r'$r/a = 0.4$')
ax3.scatter(met_data[:, 9], chi_data[:, 2], marker='s', s=150, facecolor='None', edgecolor='tab:green', label=r'$r/a = 0.6$')
ax4.scatter(met_data[:, 9], chi_data[:, 3], marker='^', s=150, facecolor='None', edgecolor='tab:red', label=r'$r/a = 0.8$')

ax1.scatter([1.], [chi_qhs_data[0]], marker='*', s=500, c='k', edgecolor='w')
ax2.scatter([1.], [chi_qhs_data[1]], marker='*', s=500, c='k', edgecolor='w')
ax3.scatter([1.], [chi_qhs_data[2]], marker='*', s=500, c='k', edgecolor='w')
ax4.scatter([1.], [chi_qhs_data[3]], marker='*', s=500, c='k', edgecolor='w')

# Axis Limits #
ax1.set_xlim(0.955, 1.08)
ax1.set_ylim(1e-1, 1.5e2)
ax1.set_yscale('log')

# Axis Labels #
ax1.set_ylabel(r'$D \ (\%)$')
ax3.set_ylabel(r'$D \ (\%)$')

ax3.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax4.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

# Axis Legends #
ax1.legend()
ax2.legend()
ax3.legend()
ax4.legend()

# Axis Text #
x_txt, y_txt = 1.065, 0.15
ax1.text(x_txt, y_txt, '(a)')
ax2.text(x_txt, y_txt, '(b)')
ax3.text(x_txt, y_txt, '(c)')
ax4.text(x_txt, y_txt, '(d)')

# Axis Grid #
ax1.grid()
ax2.grid()
ax3.grid()
ax4.grid()

# Save or Show #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'D_test.png')
plt.savefig(save_path)
# plt.show()
