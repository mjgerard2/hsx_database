import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

from vmecTools.wout_files import wout_read as wr


# Import Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data_scaled.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Import well data #
hf_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'data_files', 'well_norm.h5')
with hf.File(hf_path, 'r') as hf_:
    well_qhs = -hf_['QHS'][()]
    well = np.empty((met_data.shape[0], well_qhs.shape[0]))
    for mdx, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        well[mdx] = -hf_[conID][()]

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 22}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 26

# Plotting Axis #
fig = plt.figure(figsize=(10, 8))
axs = ImageGrid(fig, 111,
                nrows_ncols=(2, 2),
                axes_pad=0.5,
                share_all=True,
                aspect=False)

ax1 = axs[0]
ax2 = axs[1]
ax3 = axs[2]
ax4 = axs[3]

# Plot Well Data #
ax1.scatter(met_data[:, 9], well[:, 0], marker='o', s=150, facecolor='None', edgecolor='tab:red')
ax2.scatter(met_data[:, 9], well[:, 1], marker='o', s=150, facecolor='None', edgecolor='tab:red')
ax3.scatter(met_data[:, 9], well[:, 2], marker='o', s=150, facecolor='None', edgecolor='tab:red')
ax4.scatter(met_data[:, 9], well[:, 3], marker='o', s=150, facecolor='None', edgecolor='tab:red')

ax1.scatter([1.], well_qhs[0], marker='*', s=750, c='k', edgecolor='w')
ax2.scatter([1.], well_qhs[1], marker='*', s=750, c='k', edgecolor='w')
ax3.scatter([1.], well_qhs[2], marker='*', s=750, c='k', edgecolor='w')
ax4.scatter([1.], well_qhs[3], marker='*', s=750, c='k', edgecolor='w')

# Axis Limits #
# ax1.set_xlim(0.955, 1.08)
ax1.set_ylim(-.005, .035)

# Axis Labels #
ax1.set_ylabel(r'$-\frac{d^2V}{d\psi^2} \left( \frac{\psi_{\mathrm{LCFS}}^2}{V_{\mathrm{LCFS}}} \right)$')
ax3.set_ylabel(r'$-\frac{d^2V}{d\psi^2} \left( \frac{\psi_{\mathrm{LCFS}}^2}{V_{\mathrm{LCFS}}} \right)$')

ax3.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax4.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

# Axis titles #
ax1.set_title(r'$r/a = 0.2$')
ax2.set_title(r'$r/a = 0.4$')
ax3.set_title(r'$r/a = 0.6$')
ax4.set_title(r'$r/a = 0.8$')

# Axis Grids #
ax1.grid()
ax2.grid()
ax3.grid()
ax4.grid()

# Axis Text #
"""
x_txt, y_txt = 1.063, -.0035
ax1.text(x_txt, y_txt, '(a)')
ax2.text(x_txt, y_txt, '(b)')
ax3.text(x_txt, y_txt, '(c)')
ax4.text(x_txt, y_txt, '(d)')
"""
# Adjust Figure Layout #
plt.gcf().subplots_adjust(left=0.16, right=0.97)

# Plot/Save Figures #
save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'hsx_database', 'figures', 'well_norm.png')
plt.savefig(save_path)
# plt.show()
