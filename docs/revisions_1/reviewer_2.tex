\documentclass[a4paper, 12pt]{article}
\usepackage[margin=.75in]{geometry}


% AMS Packages
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{bm}

\usepackage{setspace} % for \onehalfspacing and \singlespacing macros
\onehalfspacing 

\usepackage{etoolbox}
\AtBeginEnvironment{quote}{\par\singlespacing\small}

\newcommand{\stkout}[1]{\ifmmode\text{\sout{\ensuremath{#1}}}\else\sout{#1}\fi}

\title{Author Response to Second Reviewer}

\begin{document}
	\maketitle
	
	The authors express their sincere gratitude to the reviewers for their thoughtful consideration and feedback. The changes made to the manuscript in response to their comments have undoubtedly strengthened the paper. It is our hope that all referee comments have been adequately addressed in the revised manuscript.
	
	\begin{enumerate}
		\item Abstract. The phrases ``perturbative optimization'', ``fixed operation point'', ``hierarchical set of metrics'', seem too technical for an abstract. Can you find a more	accessible way to describe this? (E.g. neighborhood of standard operating point of	HSX, multiple-stage filtering of candidate configurations...etc) \\
		
		\textbf{Response}: These phrases have been removed. Where it previously read ``A perturbative optimization approach around a fixed operation point has been applied to the Helically Symmetric eXperiment (HSX) to enable the search for microinstability-optimized magnetic-field geometries'', it now reads ``The optimization of HSX for reduced microinstability has been achieved by examining a large set of configurations within a neighborhood of the standard operating configuration.'' Similarly, the phrase ``hierarchical set of metrics'' has been changed to ``set of volume-averaged metrics''.
		
		\item Page 4, bottom. The signs of $\omega_*$ and $\omega_d$ depend on that of $\kappa_{\alpha}$, etc., so this discussion	should be modified to reflect this. \\
		
		\textbf{Response}: Considerable edits have been made to Sec.~II. With regards to the impact of $\kappa_{\alpha}$ on $\overline{\omega}_{\mathrm{de}}$, the $\overline{\omega}_{\mathrm{de}}$ curvature drive (Eq.~(1)) has been added, with a derivation included in Appendix A. Moreover, a plot of the curvature drive along a magnetic field-line has been included for QHS in Fig.~1. This shows that the $\overline{\omega}_{\mathrm{de}}$ curvature drive in HSX is dominated by the contribution from $\kappa_n/|\nabla \psi|$, which justifies the focus on $\kappa_n$ in the TEM metrics.
		
		\item Page 5. The quantity $\mathcal{F}_{\kappa}$ is intuitive in some ways, but it seems to count ``bad curvature'' as ``good'' in the regions with fewer trapped particles because $B - \langle B \rangle$	has a sign change where $B$ is equal to $\langle B \rangle$. Is there a reason for this choice? Why
		not use $B - B_{\mathrm{max}}$ instead, for instance, or trapped particle fraction? \\
		
		\textbf{Response}: Thank you for the recommendation. $\mathcal{F}_{\kappa}$ has been changed to $\mathcal{B}_{\kappa}$, defined in Eq.~(3), and the text has been modified throughout the paper to reflect this change. This has helped to make the physical interpretation of the metric more apparent, as $\mathcal{B}_{\kappa}$ can be treated as a magnetic-well weighted volume-average of $\kappa_n$.
		
		\item Page 6. Two quantities defined in (5) and (6) are somewhat redundant in the sense that $\epsilon_{\mathrm{eff}}$ is zero in the limit that quasi-symmetry is satisfied. Can the authors explain their reasons for including both? \\
		
		\textbf{Response}: Originally we wanted to convince ourselves that the two metrics really are correlated. But you are correct, the confirmation that they are is neither new or interesting. The new version considers only the symmetry breaking ratio $\mathcal{Q}$, and the text has been modified throughout to reflect this change.
		
		\item Page 7. ``the reduction of the current is only considered for two coils at a time in order to limit the effort needed in the event such a configuration is pursued in future experimental campaigns.'' How does this reduce effort? \\
		
		\textbf{Response}: This has been clarified in the new manuscript. Previously, on page 7, it said
		
		\begin{quote}
			the reduction of the current is only considered for two coils at a time in order to limit the effort needed in the event such a configuration is pursued in future experimental campaigns.
		\end{quote}
		
		In the revised version, on page 8 it reads
		
		\begin{quote}
			the reduction of the current is only considered for two coils at a time. This is because, at present, all main coils are connected in series, so reducing the current in any one of them will require a shunt resistor to by-pass that particular coil. Restricting the analysis to no more than two main coil-current modifications at a time limits the effort needed in the event such a configuration is pursued in future experimental campaigns.
		\end{quote}
		
		\item Page 7. ``all six auxiliary coil currents are permuted across states'' Does this imply that the auxiliary currents are allowed to vary simultaneously, even though the main currents are only allowed to change in pairs? Reason for this difference? \\
		
		\textbf{Response}: That is correct. Both the main and auxiliary coils are connected in series, but the current leads for the auxiliary coils are removable while the leads for the main coils are not. This flexibility with the auxiliary coils allows them to be connected with either positive or negative polarity with some base current supply, or disconnected for no current. 
		
		\item Page 9. ``Clearly, the distribution of normal curvature over the flux surfaces is modified by elongating the plasma boundary in HSX.'' It seems that the metric $\mathcal{F}_{\kappa}$ does not only target the distribution of curvature, but the amplitude as well, which must have consequences for stability. Is this intentional? Is some change in the amplitude also observed? If not is there a reason that the optimization manages to avoid this.	What do the authors conclude about the relative roles of $\mathcal{G}_{\psi}$ and $\mathcal{F}_{\kappa}$ in explaining the optimization found? \\
		
		\textbf{Response}: An increase in the amplitude of $\kappa_n$ has been observed in a few configurations with increasing $\mathcal{F}_{\kappa}$ and the new metric $\mathcal{B}_{\kappa}$, but the impact of this increase is not clear, and will be the topic of future work. In addition to redefining $\mathcal{F}_{\kappa}$ as $\mathcal{B}_{\kappa}$, $\mathcal{G}_{\psi}$ has been redefined as $\mathcal{C}_n$, the latter of which is the volume-averaged normal curvature contribution to the $\overline{\omega}_{\mathrm{de}}$ curvature drive, with $\mathcal{C}_n$ now defined in Eq.~(4). Redefining these metrics in this way has led to greater transparency in the impact of elongation. Therefore, in Sec.~VI, the relative roles of $\mathcal{B}_{\kappa}$ and $\mathcal{C}_n$ has been addressed by replacing 
		
		\begin{quote}
			. . . increasing elongation is consistent with the expansion of positive normal curvature into the outboard mid-plane, which was also shown to occur with increasing flux-surface elongation in Fig.~5. Since this relationship between flux-surface elongation and $\kappa_{\mathrm{n}}$ was identified with the use of flux-surface-averaged metrics $\mathcal{F}_{\kappa}$ and $\mathcal{G}_{\psi}$, the linear \textsc{GENE} results demonstrate that these metrics are sufficiently sensitive to changes in the distribution of $\kappa_{\mathrm{n}}$ to predict with reasonable accuracy which magnetic field configurations will exhibit modifications in TEM growth rates.
		\end{quote}
	
		with
		
		\begin{quote}
			Comparing Fig.~12 with Fig.~4, the reduction in growth rate with increasing elongation means the growth rate is also reduced with decreasing $|\mathcal{B}_{\kappa}|$ and increasing $|\mathcal{C}_n|$. This suggests that the shift of negative $\kappa_n$ away from the magnetic wells, resulting in lower values of $|\mathcal{B}_{\kappa}|$, has a more significant stabilizing effect than the volume-averaged increase in the $\kappa_n$ curvature drive in the electron diamagnetic direction described by the increase in $|\mathcal{C}_n|$. Therefore, in HSX, TEM stability appears to depend more on the the drifts of the deeply trapped particles than the volume averaged drifts.
		\end{quote}
		
		\item Page 18. ``a more robust analysis of the reduction in growth rates is warranted.'' What potential problems/limitations do you foresee in continuing to using these	metrics? Do you have reason to expect them to perform worse with optimization outside of the ``safe region'' defined by the ``fixed-point''? \\
		
		\textbf{Response}: To address this comment, the following excerpt has been added on to the end of Sec.~VI page 19
		
		\begin{quote}
			While this result justifies the use of these metrics in the perturbative optimization approach, it is unclear how they would perform in a gradient-descent based optimization. Furthermore, in a general stellarator equilibrium, these metrics should be modified to include the additional terms in the curvature drive Eq.~(1), since $C_d$ will not, in general, be dominated by the $\kappa_n/|\nabla\psi|$ component.
		\end{quote}
		
		\item Conclusions. No mention is made about nonlinear simulations. With 100 GK simulations	made already, one might expect that a nonlinear simulation must have already been attempted. Do the authors wish to comment about nonlinear results, or forthcoming	studies? \\
		
		\textbf{Response}: Nonlinear simulations are ongoing. Because of the inclusion of kinetic ions and electrons, a considerable amount of computing time is required to identify a set of converged numerical parameters across configurations with different elongation ratios. Also, there are surprising discrepancies between nonlinear simulations with $\beta = 0$ and $\beta = 5\times 10^{-4}$. The resolution of these issues is ongoing, and warrant a greater level of analysis than what we feel could be included in this paper. In the conclusion it says, ``At present, nonlinear simulations of select configurations are ongoing, and will be reported in a separate publication, along with additional analysis regarding the observed linear stabilization.''
		
	\end{enumerate}
	
\end{document}