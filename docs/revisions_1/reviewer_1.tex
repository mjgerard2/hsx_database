\documentclass[a4paper, 12pt]{article}
\usepackage[margin=.75in]{geometry}

% AMS Packages
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{bm}

\usepackage{setspace} % for \onehalfspacing and \singlespacing macros
\onehalfspacing 

\usepackage{etoolbox}
\AtBeginEnvironment{quote}{\par\singlespacing\small}

\newcommand{\stkout}[1]{\ifmmode\text{\sout{\ensuremath{#1}}}\else\sout{#1}\fi}

\title{Author Response to First Reviewer}

\begin{document}
	\maketitle
	
	The authors express their sincere gratitude to the reviewers for their thoughtful consideration and feedback. The changes made to the manuscript in response to their comments have undoubtedly strengthened the paper. It is our hope that all referee comments have been adequately addressed in the revised manuscript.
	
	\begin{enumerate}
		\item It needs to be stated in the paper how vmec’s ``phiedge'' parameter was varied (if at all) as the coil currents were varied. For free-boundary calculations, phiedge essentially sets the size of the vmec domain. If the total coil currents were reduced while phiedge was fixed, the vmec domain expands. To keep the vmec volume fixed between configurations, phiedge should probably be varied proportional to the sum of the coil currents. If you didn’t do this and instead kept phiedge fixed, I don’t suggest re-doing the entire analysis, but it should at least be discussed in the text. \\
		
		\textbf{Response}: Thank you for this important comment. ``phiedge'' was held fixed, and this has been addressed in the revised version. Originally, on page 6 the manuscript read
		
		\begin{quote}
			a coil-current database has been produced using the 3D magnetohydrodynamic (MHD) equilibrium code VMEC [29] in free-boundary mode to calculate the magnetic-field structure assuming zero equilibrium $\beta$
		\end{quote}
	
		Now, on page 7 it reads
		
		\begin{quote}
			a coil-current database has been produced using the 3D magnetohydrodynamic (MHD) equilibrium code VMEC [30], which was run in free-boundary mode to calculate the magnetic-field structure assuming zero equilibrium $\beta$ and fixed $\psi_{\mathrm{LCFS}}$.
		\end{quote}
	
		Regarding the impact this has on the volume and magnetic field strength, the following comments were added at the beginning of Sec.~IV on page 11
			
		\begin{quote}
			One important consequence of running VMEC in free boundary mode with a fixed $\psi_{\mathrm{LCFS}}$ is that the volume enclosed by each configuration is related to the total current in all external coils. Therefore, if the total current is reduced, the equilibrium volume will increase as the magnetic field strength is reduced. For the set of 100 configurations, the volume increase is approximately linear with elongation, going from $0.32$ to $0.41 \ \mathrm{m}^3$, while the the volume-averaged magnetic-field decreases from $1.11$ to $0.86 \ \mathrm{T}$, possibly having a deleterious effect on plasma confinement.
		\end{quote}
		
		\item In section II, there is an emphasis on $\kappa_n$, but this is not a quantity that appears in the gyrokinetic equation, so I don’t see why it is meaningful. The quantity in the gyrokinetic	equation is $\mathbf{v}_d \cdot \nabla \alpha$, which includes $\mathbf{B} \times \boldsymbol{\kappa} \cdot \nabla \alpha$ and $\mathbf{b} \times \nabla B \cdot \nabla \alpha$ (which are identical at low beta).	You refer to ref [16], but I did not see a justification in [16] for using $\kappa_n$ instead of $\mathbf{B} \times \boldsymbol{\kappa} \cdot \nabla \alpha$. Either use the correct quantity, or justify the use of $\kappa_n$ instead. (If the issue with
		the correct quantity is the nonperiodic term in $\nabla \alpha$, then I would either drop the	nonperiodic term or replace flux surface averages with integrals along a field line.) \\
		
		\textbf{Response}: Considerable edits have been made to Sec.~II. With regards to the attention placed on $\kappa_n$ on $\overline{\omega}_{\mathrm{de}}$, the $\overline{\omega}_{\mathrm{de}}$ curvature drive (Eq.~(1)) has been added, with a derivation included in Appendix A. Moreover, a plot of the curvature drive along a magnetic field-line has been included for QHS in Fig.~1. This shows that the $\overline{\omega}_{\mathrm{de}}$ curvature drive in HSX is dominated by the contribution from $\kappa_n/|\nabla \psi|$, which justifies the focus on $\kappa_n$ in the TEM metrics.
		
		\item The paper emphasizes elongation as the key quantity that varies among the
		configurations. But looking at figure 4, it appears that the volume and triangularity are strongly correlated with your measure of elongation. Why favor elongation over these	other two quantities? This should be discussed in the text. In your set of 100 GENE calculations, is the GENE growth rate better correlated with elongation than with triangularity or volume? \\
		
		\textbf{Response}: Indeed, elongation is also well correlated with triangularity, where triangularity has been calculated as described in Appendix C of the new manuscript. The text has been modified throughout the manuscript to include this observation. Regarding the correlation of the growth rates with the other metrics, the inclusion of Fig.~4 in the new manuscript shows the metrics are correlated with elongation and triangularity, and the following comment has been added in the latter half of Sec.~VI, ``Comparing Fig.~12 with Fig.~4, the reduction in growth rate with increasing elongation and triangularity means the growth rate is also reduced with decreasing $|\mathcal{B}_{\kappa}|$ and increasing $|\mathcal{C}_n|$.'' Regarding the increase in volume, see the response to comment 1.
		
		\item A natural question is whether the reduced linear growth rates translate to reduced nonlinear fluxes. It is mentioned in the conclusion that you are saving nonlinear calculations for a second paper. Nonlinear simulations would greatly strengthen this	paper, so you might consider including at least 2-3 of them here (optional). \\
		
		\textbf{Response}: Nonlinear simulations are ongoing. Because of the inclusion of kinetic ions and electrons, a considerable amount of computing time is required to identify a set of converged numerical parameters across configurations with different elongation ratios. Also, there are surprising discrepancies between nonlinear simulations with $\beta = 0$ and $\beta = 5\times 10^{-4}$. The resolution of these issues is ongoing, and warrant a greater level of analysis than what we feel could be included in this paper.
		
		\item Since currents in the non-planar coils were adjusted downward but not upward, the	configurations with lowered TEM growth rates might have weaker average field strength, which would worsen confinement. Or perhaps increased currents in the planar coils compensate – it is not knowable from the text. Can you comment in the paper on	how much $|\mathbf{B}|$ was lowered (if at all), and how much effect on confinement you expect as a result? \\
		
		\textbf{Response}: This has been addressed in the response to comment 1. The neoclassical SFINCS simulations show that the reduction of the volume-averaged magnetic-field strength with increasing elongation does not significantly increase the neoclassical heat-flux in configurations with $\mathcal{K}/\mathcal{K}^* \leq 1.05$. Additionally, in the case one of these configurations is pursued in future experimental campaigns, the coil currents will have to be scaled so as to achieve an on-axis field strength necessary for ECRH heating.
		
		\item Little justification is given for the surrogate in eq (3). By pointing to terms in the gyrokinetic equation, can you provide a justification? \\
		
		\textbf{Response}: Eq.~(3) has been modified based on the recommendation made in comment 7. The new metric is $\mathcal{C}_n$, which is defined in Eq.~(4). The justification for this metric is based on the curvature drive for $\overline{\omega}_{\mathrm{de}}$, which is defined in Eq.~(1) with a derivation in Appendix~A. The figures and text have been modified throughout the manuscript to reflect this change.
		
		\item In eq (3), would it not be better to use $1/g^{\psi\psi}$ instead of $g_{\psi\psi}$? The reason is that $g_{\psi\psi}$ depends on the arbitrary choice of poloidal angle – consider that the derivatives in (4)	hold theta fixed, so you get a different result if you use the VMEC angle vs PEST angle vs
		Boozer angle. The squared inverse distance between flux surfaces is $g_{\psi\psi}$ and is independent of the poloidal and toroidal angles. \\
		
		\textbf{Response}: This recommendation aligns nicely with the inclusion of the $\overline{\omega}_{\mathrm{de}}$ curvature drive in Eq.~(1), therefore the $\mathcal{G}_{\psi}$ metric has been changed to the new metric $\mathcal{C}_n$, defined in Eq.~(4). This change, along with the change in $\mathcal{F}_{\kappa}$ to $\mathcal{B}_{\kappa}$, the latter of which is defined in Eq.~(3), has led to greater transparency in interpreting the results of the gyrokinetic simulations. 
		
		\item I suggest including a new figure showing the correlation between eq (2)-(3) and the	GENE linear growth for the 100 configurations. (The x axis would be eq (2), the y axis	would be the GENE growth rate, and it would be a scatter plot with 100 points. Another panel would be the same but with eq (3) as the x axis.) No new calculations would be needed. One of the main ideas in the paper is that eq (2)-(3) might be useful surrogates, and the best way to test this hypothesis is to measure correlation with the high-fidelity model (GENE), which this new plot would do. It would also be interesting to include previous proxies by Proll et al and Helander’s available energy, to see how the
		correlation of these proxies with GENE compares to the correlation of your new proxies. \\
		
		\textbf{Response}: Figure~4 has been added to the revised manuscript. This figure makes the correlation between elongation, triangularity and the volume-averaged TEM metrics more explicit for the set of 100 configurations. On page 18 in Sec.~VI the following has been added
		
		\begin{quote}
			Comparing Fig.~12 with Fig.~4, the reduction in growth rate with increasing elongation means the growth rate is also reduced with decreasing $|\mathcal{B}_{\kappa}|$ and increasing $|\mathcal{C}_n|$. This suggests that the shift of negative $\kappa_n$ away from the magnetic wells, resulting in lower values of $|\mathcal{B}_{\kappa}|$, has a more significant stabilizing effect than the volume-averaged increase in the $\kappa_n$ curvature drive in the electron diamagnetic direction described by the increase in $|\mathcal{C}_n|$. Therefore, in HSX, TEM stability appears to depend more on the the drifts of the deeply trapped particles than the volume averaged drifts.
		\end{quote}
	
		For the reviewer's consideration, the requested figure has been included at the bottom of this document as Fig.~\ref{fig:gene_data}. Considering that Fig.~4 in the revised manuscript demonstrates the correlation of all quantities of interest, it is the opinion of the authors that replacing Fig.~12 in the manuscript with Fig.~\ref{fig:gene_data} shown here does not provide significantly more information. Regarding the use of the TEM available energy metric, a collaboration with R. Mackenbach is ongoing, and will be reported on in future publications.
		
		\item I didn’t understand why currents were reduced in only 2 of the non-planar coils and not more. Maybe there is a reason to do with how the currents would be adjusted	experimentally. Can you elaborate? \\
		
		\textbf{Response}: This has been clarified in the new manuscript. Previously, on page 7, it said
		
		\begin{quote}
			the reduction of the current is only considered for two coils at a time in order to limit the effort needed in the event such a configuration is pursued in future experimental campaigns.
		\end{quote}
		
		In the revised version, on page 8 it reads
		
		\begin{quote}
			the reduction of the current is only considered for two coils at a time. This is because, at present, all main coils are connected in series, so reducing the current in any one of them will require a shunt resistor to by-pass that particular coil. Restricting the analysis to no more than two main coil-current modifications at a time limits the effort needed in the event such a configuration is pursued in future experimental campaigns.
		\end{quote}
		
		\item ``the region of positive normal curvature extends further into the outboard side of the flux surface…'' It is not clear why ``outboard'' matters per se – what presumably matters more directly is the field strength, which cannot be seen from the figure. Would it not be more meaningful to show $|\mathbf{B}|$ and $\mathbf{B} \times \boldsymbol{\kappa} \cdot \nabla \alpha$ along a field line, to show that $\mathbf{B} \times \boldsymbol{\kappa} \cdot \nabla \alpha$ is on average more positive in a typical trapping well? \\
		
		\textbf{Response}: The comment ``the region of positive normal curvature extends further into the outboard side of the flux surface . . '' has been removed. It was made in reference to Fig.~4, which has been replaced by Fig.~5. Figure~5 is used only to demonstrate the flux-surface shaping that occurs within the set of 100 configurations, and does not show $\kappa_n$. Regarding a magnetic field-line plot, one is provided for QHS in Fig.~1, but similar plots for configurations with low and high elongation/triangularity are difficult to interpret with regards to TEM stability, and have therefore been omitted.
		
		\item For the quantity $Q_{\mathrm{es}}$, is this the sum of the heat fluxes from electrons and ions, or just one species? Also what does subscript ``es'' stand for? \\
		
		\textbf{Response}: $Q_{\mathrm{es}}$ is the ion-root electron heat-flux. This has been addressed in the text on Page 12. Where it used to read ``The flux-surface-averaged ion-root heat-flux $\langle Q_{\mathrm{es}} \rangle$'', it now read ``The flux-surface-averaged ion-root electron heat-flux $\langle Q_{\mathrm{e}} \rangle$''. The subscript has been changed to $\mathrm{e}$ to indicate that we are considering just the electron heat-flux.
		
		\item The phrase ``fixed-point optimization'' is used in several places. I recommend avoiding this phrase – it’s confusing because ``fixed-point'' in applied mathematics refers to unrelated topics, Picard iteration or non-floating-point decimal numbers. \\
		
		\textbf{Response}: All references throughout the text to a ``fixed-point optimization'' have been changed to ``perturbative optimization''.
		
		\item Page 10: ``4D Fokker-Planck collision operator'' A collision operator acts in velocity space and so in $\leq 3$ dimensions. \\
		
		\textbf{Response}: You are correct, this was an error. On pg.~10 the original text read 
		
		\begin{quote}
			SFINCS solves the drift-kinetic equation, Eq.~(19) in Ref.~[32], with a linearized 4D Fokker-Planck collision operator on an individual flux surface with a specified radial electric field $E_r$. 
		\end{quote}
			
		It now reads
		
		\begin{quote}
			SFINCS solves the 4D linearized drift-kinetic equation, Eq.~(16) and (27) in Ref.~[31], with a linearized Fokker-Planck collision operator on an individual flux surface with a specified radial electric field $E_r$
		\end{quote}
	
		The equations solved by SFINCS were also incorrectly cited, so the changes in the text reflect that correction as well.
		
		\item Page 16: ``$\psi\psi_{LCFS} = 0.5$'' missing / \\
		
		\textbf{Response}: The missing / has been added. Thank you for catching that.
		
		\item Page 18: ``$L_{\chi} = \chi/|\nabla\chi|$'' is false: the left side is a flux function whereas the right side varies along a field line. \\
		
		\textbf{Response}: The following line has been added for clarification, ``$L_{\chi} = \chi / |\nabla \chi|$, with $\chi$ the zeroth-order flux-functions for density or temperature for either electron or ion species.''
				
		\item Page 18: ``positive normal curvature into the outboard mid-plane''. But the curvature is negative on the outboard midplane – maybe you didn’t mean to include ``midplane''. \\
		
		\textbf{Response}: Fig.~4 has been replaced by Fig.~5 in the manuscript. The new figure is used only to visualize the shape of the flux surfaces as elongation (and triangularity) are increased. All commentary on the reduction in growth rates observed with increasing elongation is related back to the new metrics $\mathcal{B}_{\kappa}$ and $\mathcal{C}_n$.
		
		\item Introduction: consider mentioning that you focus on TEM instead of ITG because Ti in HSX is small. \\
		
		\textbf{Response}: The following sentence has been added on pg.~3, ``Regarding the ion-temperature gradient (ITG) mode, the experimentally observed gradients do not exceed the necessary critical gradient for destabilization [17, 18]. Therefore ITGs are neglected in the present analysis.''
	\end{enumerate}

	\begin{figure}
		\centering
		\includegraphics[width=\textwidth, keepaspectratio]{../figures/gene_data_NewMet.png}
		\caption{TEM growth rates and real frequencies plotted as a function of the TEM metrics and elongation. \label{fig:gene_data}}
	\end{figure}

\end{document}